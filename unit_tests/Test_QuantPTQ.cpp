/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <catch2/catch_test_macros.hpp>

TEST_CASE("[tmp] basic test") {
    REQUIRE(true == true);
}

// TEST_CASE("[aidge_module_template/ref_cpp/quantization] PTQ : Quantize Graph") {

//     std::shared_ptr<GraphView> g1 = std::make_shared<GraphView>("TestGraph");

//     bool verbose = true;

//     std::shared_ptr<Node> myConv1 = Conv(3,4,{3,3}, "myConv1");
//     myConv1->getOperator()->setDatatype(DataType::Float32);
//     myConv1->getOperator()->setBackend("cpu");

//     Tensor myWeights = Array4D<float,4,3,3,3> {
//         {
//             {
//                 {{  0.,   1.,   2.},
//                  {  3.,   4.,   5.},
//                  {  6.,   7.,   8.}},
//                 {{  9.,  10.,  11.},
//                  { 12.,  13.,  14.},
//                  { 15.,  16.,  17.}},
//                 {{ 18.,  19.,  20.},
//                  { 21.,  22.,  23.},
//                  { 24.,  25.,  26.}}
//             },
//             {
//                 {{ 27.,  28.,  29.},
//                  { 30.,  31.,  32.},
//                  { 33.,  34.,  35.}},
//                 {{ 36.,  37.,  38.},
//                  { 39.,  40.,  41.},
//                  { 42.,  43.,  44.}},
//                 {{ 45.,  46.,  47.},
//                  { 48.,  49.,  50.},
//                  { 51.,  52.,  53.}}
//             },
//             {
//                 {{ 54.,  55.,  56.},
//                  { 57.,  58.,  59.},
//                  { 60.,  61.,  62.}},
//                 {{ 63.,  64.,  65.},
//                  { 66.,  67.,  68.},
//                  { 69.,  70.,  71.}},
//                 {{ 72.,  73.,  74.},
//                  { 75.,  76.,  77.},
//                  { 78.,  79.,  80.}}
//             },
//             {
//                 {{ 81.,  82.,  83.},
//                  { 84.,  85.,  86.},
//                  { 87.,  88.,  89.}},
//                 {{ 90.,  91.,  92.},
//                  { 93.,  94.,  95.},
//                  { 96.,  97.,  98.}},
//                 {{ 99., 100., 101.},
//                  {102., 103., 104.},
//                  {105., 106., 107.}}
//             }
//         }
//     };
//     Tensor myBias = Array1D<float,4> {{7.,0.,9.,0.}};

//     std::shared_ptr<Tensor> myInput =
//             std::make_shared<Tensor>(
//                 Array4D<float,2,3,5,5> {
//                     {
//                         {
//                             {{  0.,   1.,   2.,   3.,   4.},
//                             {  5.,   6.,   7.,   8.,   9.},
//                             { 10,  11,  12,  13,  14.},
//                             { 15,  16,  17,  18,  19.},
//                             { 20,  21,  22,  23,  24.}},

//                             {{ 25,  26.,  27.,  28.,  29.},
//                             { 30.,  31.,  32.,  33.,  34.},
//                             { 35.,  36.,  37.,  38.,  39.},
//                             { 40.,  41.,  42.,  43.,  44.},
//                             { 45.,  46.,  47.,  48.,  49.}},

//                             {{ 50.,  51.,  52.,  53.,  54.},
//                             { 55.,  56.,  57.,  58.,  59.},
//                             { 60.,  61.,  62.,  63.,  64.},
//                             { 65.,  66.,  67.,  68.,  69.},
//                             { 70.,  71.,  72.,  73.,  74.}}
//                         },
//                         {
//                             {{ 75.,  76.,  77.,  78.,  79.},
//                             { 80.,  81.,  82.,  83.,  84.},
//                             { 85.,  86.,  87.,  88.,  89.},
//                             { 90.,  91.,  92.,  93.,  94.},
//                             { 95.,  96.,  97.,  98.,  99.}},

//                             {{100, 101, 102, 103, 104.},
//                             {105, 106, 107, 108, 109.},
//                             {110, 111, 112, 113, 114.},
//                             {115, 116, 117, 118, 119.},
//                             {120, 121, 122, 123, 124.}},

//                             {{125, 126, 127, 128, 129.},
//                             {130, 131, 132, 133, 134.},
//                             {135, 136, 137, 138, 139.},
//                             {140, 141, 142, 143, 144.},
//                             {145, 146, 147, 148, 149.}}
//                         }
//                     }
//                 }
//             );

//     auto dataProvider = Producer(myInput, "dataProvider");
//     Tensor myOutput = Array4D<float,2,4,3,3> {
//         {
//             {
//                 {{ 15226.,  15577.,  15928.},
//                  { 16981.,  17332.,  17683.},
//                  { 18736.,  19087.,  19438.}},
//                 {{ 37818.,  38898.,  39978.},
//                  { 43218.,  44298.,  45378.},
//                  { 48618.,  49698.,  50778.}},
//                 {{ 60426.,  62235.,  64044.},
//                  { 69471.,  71280.,  73089.},
//                  { 78516.,  80325.,  82134.}},
//                 {{ 83016.,  85554.,  88092.},
//                  { 95706.,  98244., 100782.},
//                  {108396., 110934., 113472.}}
//             },
//             {
//                 {{ 41551.,  41902.,  42253.},
//                  { 43306.,  43657.,  44008.},
//                  { 45061.,  45412.,  45763.}},
//                 {{118818., 119898., 120978.},
//                  {124218., 125298., 126378.},
//                  {129618., 130698., 131778.}},
//                 {{196101., 197910., 199719.},
//                  {205146., 206955., 208764.},
//                  {214191., 216000., 217809.}},
//                 {{273366., 275904., 278442.},
//                  {286056., 288594., 291132.},
//                  {298746., 301284., 303822.}}
//             }
//         }
//     };
//     myConv1->getOperator()->input(0) = *myInput;
//     myConv1->getOperator()->input(1) = myWeights;
//     myConv1->getOperator()->input(2) = myBias;
//     myConv1->getOperator()->computeOutputDims();
//     myConv1->getOperator()->addHook("output_range");

//     dataProvider->getOperator()->setDatatype(DataType::Float32);
//     dataProvider->getOperator()->setBackend("cpu");
//     dataProvider->addChild(myConv1, 0);

//     g1->add(myConv1);

//     std::shared_ptr<Node> myReLU1 = ReLU("ReLu1");
//     myReLU1->getOperator()->setDatatype(DataType::Float32);
//     myReLU1->getOperator()->setBackend("cpu");
//     myReLU1->getOperator()->computeOutputDims();
//     myReLU1->getOperator()->addHook("output_range");
//     g1->addChild(myReLU1);
//     g1->setBackend("cpu");
//     g1->forwardDims();

//     //check hook functioning
//     SequentialScheduler scheduler(g1);
//     scheduler.forward();

//     float max_output_conv = std::static_pointer_cast<OutputRange>(myConv1->getOperator()->getHook("output_range"))->output(0);
//     if(verbose) {
//         printf("[hook] OutputRange(forward) :: Conv output max: "
//                        "\x1b[1;37m"
//                        "%f"
//                        "\n",
//                        max_output_conv);

//     }

//     float max_output_relu = std::static_pointer_cast<OutputRange>(myReLU1->getOperator()->getHook("output_range"))->getOutput(0);
//     if(verbose) {
//                 printf("[hook] OutputRange(forward) :: ReLU output max: "
//                        "\x1b[1;37m"
//                        "%f"
//                        "\n",
//                        max_output_relu);
//     }

//     //no need to do this anymore, forward does it autimatically now ...
//     //scheduler.generateScheduling(true);
//     std::vector<std::shared_ptr<Node>> ordered_graph_view = scheduler.getStaticScheduling();

//     printf("Going to quantize network :\n");

//     quantizeNetwork(ordered_graph_view, 8, verbose);

//     printf("After quantize network !!! \n");

//     if(verbose) {
//         printf("Graph after quantization :\n");
//         for (const std::shared_ptr<Node>& nodePtr : g1->getNodes()) {
//             printf("\t- node type: "
//                        "\x1b[1;37m"
//                        "%s"
//                        " , node name: "
//                         "\x1b[1;37m"
//                        "%s"
//                        "\n",
//                        (nodePtr->type()).c_str(), (nodePtr->name()).c_str());
//         }
//     }

//     SequentialScheduler scheduler_v2(g1);

//     scheduler_v2.forward();
//     scheduler_v2.generateScheduling(false);
//     std::vector<std::shared_ptr<Node>> ordered_graph_view_v2 = scheduler_v2.getStaticScheduling();

//     if(verbose) {
//         printf("Ordered graph after quantization :\n");
//         for (const std::shared_ptr<Node>& nodePtr : ordered_graph_view_v2) {
//             printf("\t- node type: "
//                        "\x1b[1;37m"
//                        "%s"
//                        " , node name: "
//                         "\x1b[1;37m"
//                        "%s"
//                        "\n",
//                        (nodePtr->type()).c_str(), (nodePtr->name()).c_str());
//         }
//     }


// }