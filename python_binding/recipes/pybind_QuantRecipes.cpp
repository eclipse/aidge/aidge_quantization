/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

#include "aidge/recipes/QuantRecipes.hpp"
//#include "aidge/hook/Hook.hpp"
#include "aidge/graph/GraphView.hpp"

namespace py = pybind11;

namespace Aidge {

void init_QuantRecipes(py::module &m) {

    m.def("pop_softmax", &popSoftMax, py::arg("network"));
    m.def("insert_batchnorm_nodes", &insertBatchNormNodes, py::arg("network"));
    m.def("sanitize_node_names", &sanitizeNodeNames, py::arg("network"));
}

} // namespace Aidge
