#include <pybind11/pybind11.h>
#include "aidge/utils/sys_info/QuantizationVersionInfo.hpp"

namespace py = pybind11;
namespace Aidge {
void init_QuantizationVersionInfo(py::module& m){
    m.def("show_version", &showQuantizationVersion);
    m.def("get_project_version", &getQuantizationProjectVersion);
    m.def("get_git_hash", &getQuantizationGitHash);
}
}
