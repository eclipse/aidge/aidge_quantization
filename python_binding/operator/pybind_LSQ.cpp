/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <pybind11/pybind11.h>

#include "aidge/data/Tensor.hpp"
#include "aidge/operator/LSQ.hpp"
#include "aidge/operator/OperatorTensor.hpp"

//#include "aidge/backend/cuda/operator/LSQImpl.hpp"
//#include "aidge/backend/cuda/operator/LSQImpl_CUDA_kernels.hpp"

namespace py = pybind11;
namespace Aidge {

void init_LSQ(py::module& m) {
    py::class_<LSQ_Op, std::shared_ptr<LSQ_Op>, OperatorTensor>(m, "LSQOp", py::multiple_inheritance())
    .def(py::init<const std::pair<int, int>&>(), py::arg("range") = std::pair<int, int>{0, 255})
    .def_static("get_inputs_name", &LSQ_Op::getInputsName)
    .def_static("get_outputs_name", &LSQ_Op::getOutputsName);
    declare_registrable<LSQ_Op>(m, "LSQOp");
    m.def("LSQ", &LSQ, py::arg("range") = std::pair<int, int>{0, 255}, py::arg("name") = "");
}
}  // namespace Aidge
