/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <pybind11/pybind11.h>

#include "aidge/data/Tensor.hpp"
#include "aidge/operator/SAT/DoReFa.hpp"
#include "aidge/operator/OperatorTensor.hpp"

namespace py = pybind11;
namespace Aidge {

void init_DoReFa(py::module& m) {
    py::enum_<DoReFaMode>(m, "DoReFaMode")
        .value("Default", DoReFaMode::Default)
        .value("Symmetric", DoReFaMode::Symmetric)
        .export_values();

    py::class_<DoReFa_Op, std::shared_ptr<DoReFa_Op>, OperatorTensor>(m, "DoReFaOp", py::multiple_inheritance())
    .def(py::init<size_t, DoReFaMode>(), py::arg("range") = 255, py::arg("mode") = DoReFaMode::Default)
    .def_static("get_inputs_name", &DoReFa_Op::getInputsName)
    .def_static("get_outputs_name", &DoReFa_Op::getOutputsName);
    declare_registrable<DoReFa_Op>(m, "DoReFaOp");
    m.def("DoReFa", &DoReFa, py::arg("range") = 255, py::arg("mode") = DoReFaMode::Default, py::arg("name") = "");
}
}  // namespace Aidge
