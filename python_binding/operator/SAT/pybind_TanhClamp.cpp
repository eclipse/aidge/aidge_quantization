/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <pybind11/pybind11.h>

#include "aidge/data/Tensor.hpp"
#include "aidge/operator/SAT/TanhClamp.hpp"
#include "aidge/operator/OperatorTensor.hpp"

namespace py = pybind11;
namespace Aidge {

void init_TanhClamp(py::module& m) {
    py::class_<TanhClamp_Op, std::shared_ptr<TanhClamp_Op>, OperatorTensor>(m, "TanhClampOp", py::multiple_inheritance())
    .def(py::init<>())
    .def_static("get_inputs_name", &TanhClamp_Op::getInputsName)
    .def_static("get_outputs_name", &TanhClamp_Op::getOutputsName);
    declare_registrable<TanhClamp_Op>(m, "TanhClampOp");
    m.def("TanhClamp", &TanhClamp, py::arg("name") = "");
}
}  // namespace Aidge
