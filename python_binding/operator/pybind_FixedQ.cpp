/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <pybind11/pybind11.h>

#include "aidge/data/Tensor.hpp"
#include "aidge/operator/FixedQ.hpp"
#include "aidge/operator/OperatorTensor.hpp"

namespace py = pybind11;
namespace Aidge {

void init_FixedQ(py::module& m) {
    py::class_<FixedQ_Op, std::shared_ptr<FixedQ_Op>, OperatorTensor>(m, "FixedQOp", py::multiple_inheritance())
    .def(py::init<std::size_t, float, bool>(), py::arg("nb_bits"), py::arg("span"), py::arg("is_output_unsigned"))
    .def_static("get_inputs_name", &FixedQ_Op::getInputsName)
    .def_static("get_outputs_name", &FixedQ_Op::getOutputsName);
    declare_registrable<FixedQ_Op>(m, "FixedQOp");
    m.def("FixedQ", &FixedQ, py::arg("nb_bits") = 8, py::arg("span") = 4.0f, py::arg("is_output_unsigned") = false, py::arg("name") = "");
}
}  // namespace Aidge