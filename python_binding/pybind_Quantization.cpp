/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

#include "aidge/backend/QuantizationCPU.hpp"

#ifdef CUDA_COMPILER_VERSION
    #include "aidge/backend/QuantizationCUDA.hpp"
#endif

namespace py = pybind11;

namespace Aidge
{

// operators
void init_FixedQ(py::module& m);
void init_LSQ(py::module& m);
void init_TanhClamp(py::module& m);
void init_DoReFa(py::module& m);

// quantization routines
void init_PTQ(py::module &m);
void init_QAT_FixedQ(py::module &m);
void init_QAT_LSQ(py::module &m);
void init_QuantRecipes(py::module &m);

void init_QuantizationVersionInfo(py::module &m);

PYBIND11_MODULE(aidge_quantization, m)
{
    init_FixedQ(m);
    init_LSQ(m);
    init_TanhClamp(m);
    init_DoReFa(m);

    init_PTQ(m);
    init_QAT_FixedQ(m);
    init_QAT_LSQ(m);
    init_QuantRecipes(m);
    init_QuantizationVersionInfo(m);
}

} // namespace Aidge
