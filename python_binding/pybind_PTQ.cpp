/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

#include <string>

#include "aidge/quantization/PTQ/Clipping.hpp"
#include "aidge/quantization/PTQ/CLE.hpp"
#include "aidge/quantization/PTQ/PTQ.hpp"

#include "aidge/graph/GraphView.hpp"

namespace py = pybind11;

namespace Aidge {
void init_PTQ(py::module &m) {

    py::enum_<Clipping>(m, "Clipping", "Kind of clipping policy to apply during the activation quantization")
        .value("MAX", Clipping::MAX)
        .value("MSE", Clipping::MSE)
        .value("AA" , Clipping::AA)
        .value("KL" , Clipping::KL);

    m.def("check_architecture", &checkArchitecture, py::arg("network"),
    R"mydelimiter(
    Determine whether an input GraphView can be quantized or not.
    :param network: The GraphView to be checked.
    :type network: :py:class:`aidge_core.GraphView`
    :return: True if the GraphView can be quantized, else False.
    :rtype: bool
    )mydelimiter");

    m.def("insert_scaling_nodes", &insertScalingNodes, py::arg("network"),
    R"mydelimiter(
    Insert a scaling node after each affine node of the GraphView.
    Also insert a scaling node in every purely residual branches.
    :param network: The GraphView containing the affine nodes.
    :type network: :py:class:`aidge_core.GraphView`
    )mydelimiter");

    m.def("normalize_parameters", &normalizeParameters, py::arg("network"),
    R"mydelimiter(
    Normalize the parameters of each parametrized node, so that they fit in the [-1:1] range.
    :param network: The GraphView containing the parametrized nodes.
    :type network: :py:class:`aidge_core.GraphView`
    )mydelimiter");

    m.def("compute_ranges", &computeRanges, py::arg("network"), py::arg("input_dataset"), py::arg("scaling_nodes_only"), py::arg("use_cuda"),
    R"mydelimiter(
    Compute the activation ranges of every affine node, given an input dataset.
    :param network: The GraphView containing the affine nodes, on which the inferences are performed.
    :type network: :py:class:`aidge_core.GraphView`
    :param input_dataset: The input dataset, consisting of a vector of input samples.
    :type input_dataset: list of :py:class:`aidge_core.Tensor`
    :param scaling_nodes_only: Whether to restrain the retreival of the ranges to scaling nodes only or not
    :type scaling_nodes_only: bool
    :return: A map associating each considered node name to it's corresponding output range.
    :rtype: dict
    )mydelimiter");

    m.def("normalize_activations", &normalizeActivations, py::arg("network"), py::arg("value_ranges"),
    R"mydelimiter(
    Normalize the activations of each affine node so that they fit in the [-1:1] range.
    This is done by reconfiguring the scaling nodes, as well as rescaling the weights and biases tensors.
    :param network: The GraphView containing the affine nodes.
    :type network: :py:class:`aidge_core.GraphView`
    :param value_ranges: The node output value ranges computed over the calibration dataset.
    :type value_ranges: list of float.
    )mydelimiter");

    m.def("quantize_normalized_network", &quantizeNormalizedNetwork, py::arg("network"), py::arg("nb_bits"), py::arg("no_quant")=false, py::arg("optimize_signs"), py::arg("verbose") = false,
    R"mydelimiter(
    Quantize an already normalized (in term of parameters and activations) network.
    :param network: The GraphView to be quantized.
    :type network: :py:class:`aidge_core.GraphView`
    :param nb_bits: The desired number of bits of the quantization.
    :type nb_bits: int
    :param apply_rounding: Whether to apply the rounding operations or not.
    :type apply_rounding: bool
    :param optimize_signs: Whether to take account of the IO signs of the operators or not.
    :type optimize_signs: bool
    :param verbose: Whether to print the sign map or not.
    :type verbose: bool
    )mydelimiter");

    m.def("quantize_network", &quantizeNetwork ,py::arg("network"), py::arg("nb_bits"), py::arg("input_dataset"), py::arg("clipping_mode") = Clipping::MAX , py::arg("no_quantization") = true, py::arg("optimize_signs") = false, py::arg("single_shift") = false,  py::arg("use_cuda") = false, py::arg("verbose") = false,
    R"mydelimiter(
    Main quantization routine. Performs every step of the quantization pipeline.
    :param network: The GraphView to be quantized.
    :type network: :py:class:`aidge_core.GraphView`
    :param nb_bits: The desired number of bits of the quantization.
    :type nb_bits: int
    :param input_dataset: The input dataset on which the value ranges are computed.
    :type input_dataset: list of :py:class:`aidge_core.Tensor`
    :param clipping_mode: Type of the clipping optimization. Can be either 'MAX', 'MSE', 'AA' or 'KL'.
    :type clipping_mode: string
    :param no_quantization: Whether to truly quantize the network or not.
    :type no_quantization: bool
    :param optimize_signs: Whether to take account of the IO signs of the operators or not.
    :type optimize_signs: bool
    :param single_shift: Whether to convert the scaling factors into powers of two. If true the approximations are compensated using the previous nodes weights.
    :type single_shift: bool
    :param verbose: Whether to print internal informations about the quantization process.
    :type verbose: bool
    )mydelimiter");

    m.def("compute_histograms", &computeHistograms, py::arg("value_ranges"), py::arg("nb_bins"), py::arg("network"), py::arg("input_dataset"), py::arg("use_cuda"),
    R"mydelimiter(
    Compute the histograms of the activations of each node contained in the map of the ranges (passed as argument).
    :param value_ranges: A map associating each considered node name to its corresponding output range.
    :type value_ranges: dict
    :param nb_bins: Desired number of bins of the returned histograms.
    :type nb_bins: int
    :param network: The GraphView containing the considered nodes.
    :type network: :py:class:`aidge_core.GraphView`
    :param input_dataset: The input dataset, consisting of a list of input samples.
    :type input_dataset: list of :py:class:`aidge_core.Tensor`
    :return: A map associating each node name to it's corresponding activation histogram.
    :rtype: dict
    )mydelimiter");

    m.def("compute_me_clipping", &computeMEClipping, py::arg("histogram"), py::arg("nb_bits"), py::arg("exponent"),
    R"mydelimiter(
    Given an input activation histogram, compute the optimal clipping value in the sense of the Lp norm.
    :param histogram: The provided activation histogram.
    :type histogram: list
    :param nb_bits: The quantization number of bits.
    :type nb_bits: int
    :param exponent: The exponent of the Lp norm (e.g. 2 for the MSE).
    :type exponent: int
    :return: The optimal clipping value.
    :rtype: float
    )mydelimiter");

    m.def("compute_kl_clipping", &computeKLClipping, py::arg("histogram"), py::arg("nb_bits"),
    R"mydelimiter(
    Given an input activation histogram, compute the optimal clipping value in the sense of the KL divergence.
    :param histogram: The provided activation histogram.
    :type histogram: list
    :param nb_bits: The quantization number of bits.
    :type nb_bits: int
    :return: The optimal clipping value.
    :rtype: float
    )mydelimiter");

    m.def("adjust_ranges", &adjustRanges, py::arg("clipping_mode"), py::arg("value_ranges"), py::arg("nb_bits"), py::arg("network"), py::arg("input_dataset"), py::arg("use_cuda"), py::arg("verbose") = false,
    R"mydelimiter(
    Return a corrected map of the provided activation ranges.
    To do so compute the optimal clipping values for every node and multiply the input ranges by those values.
    The method used to compute the clippings can be eihter 'MSE', 'AA', 'KL' or 'MAX'.
    :param clipping_mode: The method used to compute the optimal clippings.
    :type clipping_mode: enum
    :param value_ranges: The map associating each affine node to its output range.
    :type value_ranges: dict
    :param nb_bits: The quantization number of bits.
    :type nb_bits: int
    :param network: The GraphView containing the considered nodes.
    :type network: :py:class:`aidge_core.GraphView`
    :param input_dataset: The input dataset, consisting of a list of input samples.
    :type input_dataset: list of :py:class:`aidge_core.Tensor`
    :param verbose: Whether to print the clipping values or not.
    :type verbose: bool
    :return: The corrected map associating to each provided node its clipped range.
    :rtype: dict
    )mydelimiter");


    m.def("compute_sign_map", &computeSignMap, py::arg("network"), py::arg("verbose") = false,
    R"mydelimiter(
    For each node, compute the sign of its input and output values.
    The goal of the routine is to maximize the number of unsigned IOs in order to double the value resolution when possible.
    :param network: The GraphView to analyze.
    :type network: :py:class:`aidge_core.GraphView`
    :param verbose: Whether to print the sign map or not.
    :type verbose: bool
    :return: A map associating a pair of signs to each node of the GraphView (a sign for the input and one for the output).
    :rtype: dict
    )mydelimiter");

    m.def("cross_layer_equalization", &crossLayerEqualization, py::arg("network"), py::arg("target_delta"),
    R"mydelimiter(
    Equalize the ranges of the nodes parameters by proceding iteratively.
    Can only be applied to single branch networks (otherwise does not edit the graphView).
    :param network: The GraphView to process.
    :type network: :py:class:`aidge_core.GraphView`
    :param target_delta: the stopping criterion (typical value : 0.01)
    :type target_delta: float
    )mydelimiter");

    m.def("get_weight_ranges", &getWeightRanges, py::arg("network"),
    R"mydelimiter(
    Compute the weight ranges of every affine nodes. Provided for debugging purposes.
    :param network: The GraphView containing the affine nodes.
    :type network: :py:class:`aidge_core.GraphView`
    :return: A map associating each affine node name to it's corresponding weight range.
    :rtype: dict
    )mydelimiter");

    m.def("clear_biases", &clearBiases, py::arg("network"),
    R"mydelimiter(
    Clear the affine nodes biases. Provided form debugging purposes.
    :param network: The GraphView containing the affine nodes.
    :type network: :py:class:`aidge_core.GraphView`
    )mydelimiter");

    m.def("dev_ptq", &devPTQ, py::arg("network"),
    R"mydelimiter(
    Developement and test routine.
    :param network: The GraphView under test.
    :type network: :py:class:`aidge_core.GraphView`
    )mydelimiter");

    m.def("prepare_network", &prepareNetwork, py::arg("network"), "prepare the network for the PTQ");

}

} // namespace Aidge
