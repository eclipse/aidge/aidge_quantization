/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

#include "aidge/quantization/QAT/QAT_FixedQ.hpp"
#include "aidge/graph/GraphView.hpp"

namespace py = pybind11;

namespace Aidge {

void init_QAT_FixedQ(py::module &m) {

    auto mQuantFixedQ = m.def_submodule("fixedq");

    mQuantFixedQ.def("insert_quantizers", &QuantFixedQ::insertQuantizers, py::arg("network"), py::arg("nb_bits"), py::arg("span"));

    mQuantFixedQ.def("insert_and_init_quantizers", &QuantFixedQ::insertAndInitQuantizers, py::arg("network"), py::arg("nb_bits"), py::arg("calibration_data"), py::arg("scale"));

    mQuantFixedQ.def("dev_qat", &QuantFixedQ::devQAT, py::arg("network"));
}
} // namespace Aidge
