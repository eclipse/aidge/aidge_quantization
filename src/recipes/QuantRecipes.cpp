/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

/*
#include "aidge/data/Tensor.hpp"
#include "aidge/graph/GraphView.hpp"
#include "aidge/graph/Node.hpp"
#include "aidge/scheduler/SequentialScheduler.hpp"
#include "aidge/scheduler/Scheduler.hpp"
#include "aidge/utils/Log.hpp"

#include "aidge/operator/Producer.hpp"
#include "aidge/operator/Mul.hpp"
#include "aidge/operator/ReLU.hpp"
#include "aidge/operator/Scaling.hpp"
*/

#include "aidge/operator/Conv.hpp"
#include "aidge/operator/BatchNorm.hpp"
//#include "aidge/quantization/PTQ/PTQ.hpp"
#include "aidge/recipes/QuantRecipes.hpp"

namespace Aidge 
{

void popSoftMax(std::shared_ptr<GraphView> graphView)
{
    for (std::shared_ptr<Node> node : graphView->getNodes()) {
        if (node->type() == "Softmax") {
            // Check if this is a leaf node ...
            if (node->getChildren().size() == 0) {
                graphView->replace({node}, {});
            }
        }
    }
}

static int getInputIndex(std::shared_ptr<Node> node, std::shared_ptr<Node> parentNode)
{
    int index = 0;
    while (node->getParent(index) != parentNode) 
        index++;
    return index;
}

void insertBatchNormNodes(std::shared_ptr<GraphView> graphView)
{
    for (std::shared_ptr<Node> parentNode : graphView->getNodes())
    {
        if (parentNode->type() == "Conv2D")
        {
            std::shared_ptr<Conv_Op<2>> convOperator = std::static_pointer_cast<Conv_Op<2>> (parentNode->getOperator());
            int nb_channels = convOperator->getInput(1)->dims()[0];
            fmt::println(" NB CHANNELS = {}", nb_channels); // TODO : remove this ...

            std::string batchnormNodeName = makeUniqueName(parentNode->name() + "_BN", graphView);
            std::shared_ptr<Node> batchnormNode = BatchNorm<2>(nb_channels, 1e-5, 0.1, false, batchnormNodeName);
            batchnormNode->getOperator()->setDataType(DataType::Float32);
            batchnormNode->getOperator()->setBackend("cpu");

            if (parentNode->getChildren().size() > 0)
            {
                // We always have one output from Affine and Add nodes, but possibly multiple childs ...
                std::vector<std::shared_ptr<Node>> nextNodes = parentNode->getChildren(0); 

                // For each node in nextNodes store the connexion index
                std::vector<int> inputIndices(nextNodes.size());
                for (std::size_t i = 0; i < nextNodes.size(); i++)
                    inputIndices[i] = getInputIndex(nextNodes[i], parentNode);
                    
                for (std::shared_ptr<Node> nextNode : nextNodes)
                    parentNode->removeChild(nextNode, 0);

                parentNode->addChild(batchnormNode, 0, 0);

                for (std::size_t i = 0; i < nextNodes.size(); i++)
                    batchnormNode->addChild(nextNodes[i], 0, inputIndices[i]);

                graphView->add(batchnormNode);
            }
            else
            {
                // Log::info(" last node reached ! ");
                graphView->addChild(batchnormNode);
            }
        }
    }
}

std::string makeUniqueName(std::string baseName, std::shared_ptr<GraphView> graphView)
{
    std::set<std::string> existingNames;
    for (std::shared_ptr<Node> node : graphView->getNodes())
        existingNames.insert(node->name());

    bool isInside = (existingNames.find(baseName) != existingNames.end());

    if (!isInside)
        return baseName;

    int index = 1;
    std::string newName = baseName;
    while (isInside)
    {
        newName = baseName + "_" + std::to_string(index);
        isInside = (existingNames.find(newName) != existingNames.end());
        index++;
    }

    return newName;
}

void sanitizeNodeNames(std::shared_ptr<GraphView> graphView)
{
    for (std::shared_ptr<Node> node : graphView->getNodes())
    {
        std::string name = node->name();
        if (node->name() == "")
            name = node->type();
        node->setName(makeUniqueName(name, graphView));
    }
}

}