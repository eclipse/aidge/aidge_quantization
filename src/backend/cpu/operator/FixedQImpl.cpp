/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <memory>
#include <vector>

#include "aidge/data/Tensor.hpp"
#include "aidge/operator/FixedQ.hpp"
#include "aidge/utils/Types.h"
#include "aidge/backend/cpu/data/GetCPUPtr.h"
#include "aidge/utils/ErrorHandling.hpp"

#include "aidge/backend/cpu/operator/FixedQImpl.hpp"
#include "aidge/backend/cpu/operator/FixedQImpl_kernels.hpp"

template<>
void Aidge::FixedQImpl_cpu::forward()
{
	const FixedQ_Op& op_ = dynamic_cast<const FixedQ_Op&>(mOp);
    std::shared_ptr<Tensor> in0 = op_.getInput(0);
    std::shared_ptr<Tensor> out0 = op_.getOutput(0);
    AIDGE_ASSERT(in0, "missing input #0");

    // Find the correct kernel type
    const auto impl = Registrar<FixedQImpl_cpu>::create(getBestMatch(getRequiredSpec()));

    // Call kernel
    impl.forward(
        op_.nbBits(),
        op_.span(),
        op_.isOutputUnsigned(),
        in0->size(),
        getCPUPtr(mOp.getRawInput(0)),
        getCPUPtr(mOp.getRawOutput(0))
    );
}


template<>
void Aidge::FixedQImpl_cpu::backward()
{
    const FixedQ_Op& op_ = dynamic_cast<const FixedQ_Op&>(mOp);
    std::shared_ptr<Tensor> gra_int0 = op_.getInput(0)->grad();
    std::shared_ptr<Tensor> gra_out0 = op_.getOutput(0)->grad();
    //AIDGE_ASSERT(out0, "missing output #0 for current {} operator", op_.type());

    // Find the correct kernel type
    const auto impl = Registrar<FixedQImpl_cpu>::create(getBestMatch(getRequiredSpec()));

    // Call kernel
    impl.backward(
        gra_int0->size(),
        getCPUPtr(gra_out0),
        getCPUPtr(gra_int0)
    );
}
