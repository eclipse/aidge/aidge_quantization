/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <memory>
#include <vector>

#include "aidge/data/Tensor.hpp"
#include "aidge/operator/LSQ.hpp"
#include "aidge/utils/Types.h"
#include "aidge/backend/cpu/data/GetCPUPtr.h"
#include "aidge/utils/ErrorHandling.hpp"

#include "aidge/backend/cpu/operator/LSQImpl.hpp"
#include "aidge/backend/cpu/operator/LSQImpl_kernels.hpp"

template<>
void Aidge::LSQImpl_cpu::forward() {
	const LSQ_Op& op_ = dynamic_cast<const LSQ_Op&>(mOp);
    std::shared_ptr<Tensor> in0 = op_.getInput(0);
    std::shared_ptr<Tensor> in1 = op_.getInput(1);
    std::shared_ptr<Tensor> out0 = op_.getOutput(0);

    // Find the correct kernel type
    auto impl = Registrar<LSQImpl_cpu>::create(getBestMatch(getRequiredSpec()));

    // Call kernel
    impl.forward(in0->size(),
        op_.range(),
        getCPUPtr(in0),
        getCPUPtr(in1),
        getCPUPtr(out0));
}
template<>
void Aidge::LSQImpl_cpu::backward() {
    const LSQ_Op& op_ = dynamic_cast<const LSQ_Op&>(mOp);
    std::shared_ptr<Tensor> in0  = op_.getInput(0);
    std::shared_ptr<Tensor> in1  = op_.getInput(1);
    std::shared_ptr<Tensor> out0  = op_.getOutput(0);
    std::shared_ptr<Tensor> gra_int0 = op_.getInput(0)->grad();
    std::shared_ptr<Tensor> gra_int1 = op_.getInput(1)->grad();
    std::shared_ptr<Tensor> gra_out0 = op_.getOutput(0)->grad();

    // Find the correct kernel type
    auto impl = Registrar<LSQImpl_cpu>::create(getBestMatch(getRequiredSpec()));

    // Call kernel
    impl.backward(
        gra_int0->size(),
        op_.range(),
        getCPUPtr(in0),
        getCPUPtr(in1),
        getCPUPtr(gra_out0),
        getCPUPtr(gra_int0),
        getCPUPtr(gra_int1));
}
