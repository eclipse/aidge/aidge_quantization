/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <memory>
#include <vector>

#include "aidge/data/Tensor.hpp"
#include "aidge/operator/SAT/TanhClamp.hpp"
#include "aidge/utils/Types.h"
#include "aidge/backend/cpu/data/GetCPUPtr.h"
#include "aidge/utils/ErrorHandling.hpp"

#include "aidge/backend/cpu/operator/SAT/TanhClampImpl.hpp"
#include "aidge/backend/cpu/operator/SAT/TanhClampImpl_kernels.hpp"

template<>
void Aidge::TanhClampImpl_cpu::forward() {

	const TanhClamp_Op& op_ = dynamic_cast<const TanhClamp_Op&>(mOp);
    std::shared_ptr<Tensor> in0 = op_.getInput(0);
    std::shared_ptr<Tensor> out0 = op_.getOutput(0);
    std::shared_ptr<Tensor> scaling = op_.getOutput(1);

    // Find the correct kernel type
    auto impl = Registrar<TanhClampImpl_cpu>::create(getBestMatch(getRequiredSpec()));

    // Call kernel
    impl.forward(
        in0->size(),
        getCPUPtr(in0),
        getCPUPtr(scaling),
        getCPUPtr(out0));
}

template<>
void Aidge::TanhClampImpl_cpu::backward() {
    const TanhClamp_Op& op_ = dynamic_cast<const TanhClamp_Op&>(mOp);
    std::shared_ptr<Tensor> in0  = op_.getInput(0);
    std::shared_ptr<Tensor> out0  = op_.getOutput(0);
    std::shared_ptr<Tensor> scaling = op_.getOutput(1);
    std::shared_ptr<Tensor> gra_int0 = op_.getInput(0)->grad();
    std::shared_ptr<Tensor> gra_out0 = op_.getOutput(0)->grad();    

    // Find the correct kernel type
    auto impl = Registrar<TanhClampImpl_cpu>::create(getBestMatch(getRequiredSpec()));

    // Call kernel
    impl.backward(
        gra_int0->size(),
        getCPUPtr(in0),
        getCPUPtr(scaling),
        getCPUPtr(gra_out0),
        getCPUPtr(gra_int0));
}
