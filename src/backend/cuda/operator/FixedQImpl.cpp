/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifdef CUDA_COMPILER_VERSION

#include <memory>
#include <vector>

#include "aidge/data/Tensor.hpp"
#include "aidge/operator/FixedQ.hpp"
#include "aidge/utils/Types.h"
#include "aidge/utils/ErrorHandling.hpp"

#include "aidge/backend/cuda/operator/FixedQImpl.hpp"
#include "aidge/backend/cuda/operator/FixedQImpl_CUDA_kernels.hpp"
#include "aidge/operator/FixedQ.hpp"

template<>
void Aidge::FixedQImpl_cuda::forward() 
{
	const FixedQ_Op& op_ = dynamic_cast<const FixedQ_Op&>(mOp);
    std::shared_ptr<Tensor> in0 = op_.getInput(0);
    std::shared_ptr<Tensor> out0 = op_.getOutput(0);

    // Find the correct kernel type
    auto impl = Registrar<FixedQImpl_cuda>::create(getBestMatch(getRequiredSpec()));

    // Call kernel
    impl.forward(
        op_.nbBits(),
        op_.span(),
        op_.isOutputUnsigned(),
        in0->size(),
        in0->getImpl()->rawPtr(),
        out0->getImpl()->rawPtr());
}

template<>
void Aidge::FixedQImpl_cuda::backward() {

    const FixedQ_Op& op_ = dynamic_cast<const FixedQ_Op&>(mOp);
    std::shared_ptr<Tensor> gra_int0 = op_.getInput(0)->grad();
    std::shared_ptr<Tensor> gra_out0 = op_.getOutput(0)->grad(); 

    // Find the correct kernel type
    auto impl = Registrar<FixedQImpl_cuda>::create(getBestMatch(getRequiredSpec()));

    // Call kernel
    impl.backward(
        gra_int0->size(),
        gra_out0->getImpl()->rawPtr(),
        gra_int0->getImpl()->rawPtr()
    );
}

#endif
