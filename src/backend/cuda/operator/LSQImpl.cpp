/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifdef CUDA_COMPILER_VERSION

#include <memory>
#include <vector>

#include "aidge/data/Tensor.hpp"
#include "aidge/operator/LSQ.hpp"
#include "aidge/utils/Types.h"
#include "aidge/utils/ErrorHandling.hpp"

#include "aidge/backend/cuda/operator/LSQImpl.hpp"
#include "aidge/backend/cuda/operator/LSQImpl_CUDA_kernels.hpp"
#include "aidge/operator/LSQ.hpp"

//template<>
void Aidge::LSQImpl_cuda::forward() {
	const LSQ_Op& op_ = dynamic_cast<const LSQ_Op&>(mOp);
    std::shared_ptr<Tensor> in0 = op_.getInput(0);
    std::shared_ptr<Tensor> in1 = op_.getInput(1);
    std::shared_ptr<Tensor> out0 = op_.getOutput(0);

    // Find the correct kernel type
    auto impl = Registrar<LSQImpl_cuda>::create(getBestMatch(getRequiredSpec()));

    // Call kernel
    impl.forward(
        in0->size(),
        op_.range(),
        in0->getImpl()->rawPtr(),
        in1->getImpl()->rawPtr(),
        out0->getImpl()->rawPtr());
}

//template<>
void Aidge::LSQImpl_cuda::backward() {
    const LSQ_Op& op_ = dynamic_cast<const LSQ_Op&>(mOp);
    std::shared_ptr<Tensor> in0  = op_.getInput(0);
    std::shared_ptr<Tensor> in1  = op_.getInput(1);
    std::shared_ptr<Tensor> out0  = op_.getOutput(0);
    std::shared_ptr<Tensor> gra_int0 = op_.getInput(0)->grad();
    std::shared_ptr<Tensor> gra_int1 = op_.getInput(1)->grad();
    std::shared_ptr<Tensor> gra_out0 = op_.getOutput(0)->grad();    

    // XXX
/*
    size_t tmp;

    cudaDeviceSetLimit(cudaLimitStackSize, 2048);
    cudaDeviceGetLimit(&tmp, cudaLimitStackSize );
    printf(" stack limit = %ld \n", tmp);

    cudaDeviceSetLimit(cudaLimitMallocHeapSize, 100000000);
    cudaDeviceGetLimit(&tmp, cudaLimitMallocHeapSize);
    printf(" heap limit = %ld \n", tmp);
*/

    if (gra_int0->size() > mWorkspaceSize) {
        // std::cout << " reallocation " << sizeof(gra_int0) << " " << gra_int0->size() << std::endl;
        if (mWorkspace != nullptr) {
            cudaFree(mWorkspace);
        }
        CHECK_CUDA_STATUS(cudaMalloc(&mWorkspace, 8 * gra_int0->size())); // XXX This must be changed !!!
        mWorkspaceSize = gra_int0->size();
    }

    // Find the correct kernel type
    auto impl = Registrar<LSQImpl_cuda>::create(getBestMatch(getRequiredSpec()));

    // Call kernel
    impl.backward(
        gra_int0->size(),
        op_.range(),
        in0->getImpl()->rawPtr(),
        in1->getImpl()->rawPtr(),
        gra_out0->getImpl()->rawPtr(),
        gra_int0->getImpl()->rawPtr(),
        gra_int1->getImpl()->rawPtr(),
        mWorkspace);
/*
    gra_int1->setBackend("cpu");
    float *castedTensor = static_cast<float *> (gra_int1->getImpl()->rawPtr());
    std::cout << castedTensor[0] << std::endl;
    gra_int1->setBackend("cuda");
*/
}

Aidge::LSQImpl_cuda::~LSQImpl_cuda() {
    if (mWorkspace != nullptr) {
        cudaFree(mWorkspace);
    }
}

#endif
