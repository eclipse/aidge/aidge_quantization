/********************************************************************************
 * Copyright (c) 2024 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include "aidge/backend/cuda/operator/FixedQImpl_CUDA_kernels.hpp"

template <class I, class O>
__global__ void FixedQImpl_cuda_forward_kernel_(
                                        std::size_t nbBits,
                                        float span_,
                                        bool isOutputUnsigned,
                                        std::size_t inputLength,
                                        const I* input,
                                        O* output) 
{
    const size_t index = blockIdx.x * blockDim.x + threadIdx.x;
    const size_t stride = blockDim.x * gridDim.x;

    //if (index >= inputLength) return;

    I span = static_cast<I> (span_);
    I stepSize = span / static_cast<I> (1 << (nbBits - 1));

    if (isOutputUnsigned)
        stepSize /= 2;

    const I upper = span - stepSize;
    const I lower = isOutputUnsigned ? 0 : -span;

    for (size_t i = index; i < inputLength; i += stride) {
        I clipped = max(lower, min(input[i], upper));
        output[i] = round(clipped / stepSize) * stepSize;
    }
} 

template <class I, class O>
void Aidge::FixedQImpl_cuda_forward_kernel(
                                        std::size_t nbBits,
                                        float span_,
                                        bool isOutputUnsigned,
                                        std::size_t inputLength,
                                        const void* input_,
                                        void* output_) 
{
    const I* input = static_cast<const I*>(input_);
    O* output = static_cast<O*>(output_);

    int blockSize = 32;
    int numBlocks = (inputLength + blockSize - 1) / blockSize;

    FixedQImpl_cuda_forward_kernel_<<<numBlocks, blockSize>>>(
                                                    nbBits,
                                                    span_,
                                                    isOutputUnsigned,
                                                    inputLength,
                                                    input,
                                                    output);

    CHECK_CUDA_STATUS(cudaPeekAtLastError());
}

template <class GI, class GO>
__global__ void FixedQImpl_cuda_backward_kernel_(    
                                            const std::size_t inputLength,
                                            const GI* grad_output,
                                            GO* grad_input) 
{
    const size_t index = blockIdx.x * blockDim.x + threadIdx.x;
    const size_t stride = blockDim.x * gridDim.x;

    //if (index >= inputLength) return;

    for (size_t i = index; i < inputLength; i += stride)
        grad_input[i] = grad_output[i];
}

template <class GI, class GO>
void Aidge::FixedQImpl_cuda_backward_kernel(                                            
                                        const std::size_t inputLength,
                                        const void* grad_output_,
                                        void* grad_input_) 

{
    const GO* grad_output = static_cast<const GO*>(grad_output_);
    GI* grad_input = static_cast<GI*>(grad_input_);

    int blockSize = 32;
    int numBlocks = (inputLength + blockSize - 1) / blockSize;

    FixedQImpl_cuda_backward_kernel_<<<numBlocks, blockSize>>>(
                                                    inputLength,
                                                    grad_output,
                                                    grad_input);

    CHECK_CUDA_STATUS(cudaPeekAtLastError());
}
