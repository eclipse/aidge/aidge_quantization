/********************************************************************************
 * Copyright (c) 2024 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include "aidge/backend/cuda/operator/LSQImpl_CUDA_kernels.hpp"

#include <thrust/device_vector.h>
#include <thrust/reduce.h>

template <class I, class O>
__global__ void LSQImpl_cuda_forward_kernel_(std::size_t inputLength,
                                            const std::pair<int, int> range,
                                            const I* input,
                                            const I* stepSize,
                                            O* output)
{
    const size_t index = blockIdx.x * blockDim.x + threadIdx.x;
    const size_t stride = blockDim.x * gridDim.x;

    //if (index >= inputLength) return;

    const O lower = static_cast<O>(range.first);
    const O upper = static_cast<O>(range.second);

    for (size_t i = index; i < inputLength; i += stride) {
        // output[i] = input[i];
        O qData = input[i] / stepSize[0];
        qData = max(lower, min(upper, qData)); 
        output[i] = rintf(qData) * stepSize[0];
    }

} 

template <class I, class O>
void Aidge::LSQImpl_cuda_forward_kernel(std::size_t inputLength,
                           const std::pair<int, int>& range,
                           const void* input_,
                           const void* stepSize_,
                           void* output_)
{
    const I* input = static_cast<const I*>(input_);
    const I* stepSize = static_cast<const I*>(stepSize_);
    O* output = static_cast<O*>(output_);

    int blockSize = 256;
    int numBlocks = (inputLength + blockSize - 1) / blockSize;

    LSQImpl_cuda_forward_kernel_<<<numBlocks, blockSize>>>(
                                                    inputLength,
                                                    range,
                                                    input,
                                                    stepSize,
                                                    output);

    CHECK_CUDA_STATUS(cudaPeekAtLastError());
}

template <class I, class GI, class GO>
__global__ void LSQImpl_cuda_backward_kernel_(const std::size_t inputLength,
                                 const std::pair<int, int> range,
                                 const I* input,
                                 const I* stepSize,
                                 const GO* grad_output,
                                 GI* grad_input,
                                 GI* /*grad_stepSize*/,
                                 GI* grad_workspace,
                                 GI gradScaleFactor) 
{
    const size_t index = blockIdx.x * blockDim.x + threadIdx.x;
    const size_t stride = blockDim.x * gridDim.x;

    //if (index >= inputLength) return;

    for (size_t i = index; i < inputLength; i += stride) {

        // grad_input[i] = grad_output[i];

        const GI fullPrecScale = input[i] / stepSize[0];
        /*****************************Data/Weights Gradient Computation************************/
        // STE method is simply applied :
        // (we accumulate the gradient instead of replacing it)
        grad_input[i] += grad_output[i] * ((fullPrecScale <= static_cast<GI>(range.first))  ? GI(0.0) :
                                           (fullPrecScale >= static_cast<GI>(range.second)) ? GI(0.0) :
                                            GI(1.0)); 

        /*****************************Step Size Gradient Computation*************************/
        GI qData = fullPrecScale;
        //1st: clip the gradient in interval [rangeMin, rangeMax] and take account of qError
        qData = ((qData <= static_cast<GI>(range.first)) ? static_cast<GI>(range.first) :
                (qData >= static_cast<GI>(range.second)) ? static_cast<GI>(range.second) :
                rintf(qData) - qData);

        //2nd: Multiplie backward data with clipped grad
        qData *= grad_output[i];
        //3rd : Multiplie by gradFactor
        qData *= gradScaleFactor; // XXX
        grad_workspace[i] = qData;
     }
}

template <class I, class GI, class GO>
void Aidge::LSQImpl_cuda_backward_kernel(const std::size_t inputLength,
                                 const std::pair<int, int>& range,
                                 const void* input_,
                                 const void* stepSize_,
                                 const void* grad_output_,
                                 void* grad_input_,
                                 void* grad_stepSize_,
                                 void* grad_workspace_)
{
    const I* input = static_cast<const I*>(input_);
    const I* stepSize = static_cast<const I*>(stepSize_);
    const GO* grad_output = static_cast<const GO*>(grad_output_);
    GI* grad_input = static_cast<GI*>(grad_input_);
    GI* grad_stepSize = static_cast<GI*>(grad_stepSize_);
    GI* grad_workspace = static_cast<GI*>(grad_workspace_);

    const GI gradScaleFactor = static_cast<GI>(1.0f / std::sqrt(inputLength * range.second));


    int blockSize = 256;
    int numBlocks = (inputLength + blockSize - 1) / blockSize;

    LSQImpl_cuda_backward_kernel_<<<numBlocks, blockSize>>>(
                                                    inputLength,
                                                    range,
                                                    input,
                                                    stepSize,
                                                    grad_output,
                                                    grad_input,
                                                    grad_stepSize,
                                                    grad_workspace,
                                                    gradScaleFactor);

    // We could to the reduction directly in the CUDA kernel. We use thrust here
    // for simplicity and foolproof-ness
    thrust::device_ptr<GI> grad_workspacePtr(grad_workspace);
    thrust::device_ptr<GI> grad_stepSizePtr(grad_stepSize);

    // We accumulate the stepSize gradient instead of replacing it 
    grad_stepSizePtr[0] += thrust::reduce(grad_workspacePtr, grad_workspacePtr + inputLength, GI(0.0));

    //printf(" step grad = %f \n", (float) grad_stepSizePtr[0]);

    CHECK_CUDA_STATUS(cudaPeekAtLastError());
}
