/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include "aidge/quantization/PTQ/PTQMetaOps.hpp"

#include <memory>
#include <string>
#include <utility>

//Operator
#include "aidge/operator/Clip.hpp"
#include "aidge/operator/Mul.hpp"
#include "aidge/operator/Round.hpp"

#include "aidge/graph/Node.hpp"
#include "aidge/graph/OpArgs.hpp"
#include "aidge/operator/MetaOperator.hpp"
#include "aidge/operator/Producer.hpp"
#include "aidge/utils/ArrayHelpers.hpp"
#include "aidge/utils/Types.h"
#include "aidge/operator/Identity.hpp"
#include "aidge/data/Tensor.hpp"
#include "aidge/operator/OperatorTensor.hpp"
#include "aidge/utils/Log.hpp"


namespace Aidge
{

std::shared_ptr<Node> Quantizer(double scalingFactor, double clipMin, double clipMax, const std::string& name)
{
    // create the nodes

    std::shared_ptr<Node> mulNode =  Mul((!name.empty()) ? name + "_MulQuant" : "");
    std::shared_ptr<Node> roundNode = Round((!name.empty()) ? name + "_RoundQuant" : "");
    std::shared_ptr<Node> clipNode = Clip((!name.empty()) ? name + "_ClipQuant" : "", clipMin, clipMax);

    // connect the scaling factor producer

    std::shared_ptr<Tensor> scalingFactorTensor = std::make_shared<Tensor>(Array1D<double, 1> {scalingFactor});
    std::shared_ptr<Node> scalingFactorProducer = addProducer<1>(mulNode, 1, {1}, "ScalingFactor");
    scalingFactorProducer->getOperator()->setOutput(0, scalingFactorTensor);

    // create the metaop graph

    std::shared_ptr<GraphView> graphView = Sequential({mulNode, roundNode, clipNode});
    std::shared_ptr<GraphView> connectedGraphView = getConnectedGraphView(mulNode); // XXX why not use the graphView ???

    // return the metaop

    std::shared_ptr<Node> metaopNode = MetaOperator("Quantizer", connectedGraphView, {}, name); // XXX alternative prototype

    return metaopNode;
}

std::shared_ptr<Node> Scaling(double scalingFactor, const std::string& name)
{
    std::shared_ptr<Tensor> scalingFactorTensor = std::make_shared<Tensor>(Array1D<double, 1> {scalingFactor});

    std::shared_ptr<Node> mulNode = Mul((!name.empty()) ? name + "_Scaling" : "");

    std::shared_ptr<Node> scalingFactorProducer = addProducer<1>(mulNode, 1, {1}, "ScalingFactor");
    scalingFactorProducer->getOperator()->setOutput(0, scalingFactorTensor);

    std::shared_ptr<GraphView> graphView  = Sequential({mulNode});
    std::shared_ptr<GraphView> connectedGraphView  = getConnectedGraphView(mulNode);

    NodePtr metaopNode = MetaOperator("Scaling", connectedGraphView, {}, name);

    return metaopNode;
}

static std::shared_ptr<Node> getSubNode(std::shared_ptr<GraphView> graphView, std::string nodeType)
{
    std::shared_ptr<Node> mulNode = nullptr;
    for(std::shared_ptr<Node> node : graphView->getNodes())
        if (node->type() == nodeType)
            mulNode = node;

    return mulNode;
}

void updateScalingFactor(std::shared_ptr<Node> metaOpNode, double scalingFactor)
{
    if(metaOpNode->type() != "Scaling" && metaOpNode->type() != "Quantizer")
        Log::warn(" Cannot update the scaling factor on Node of type {}", metaOpNode->type());

    std::shared_ptr<Tensor> scalingFactorTensor = std::make_shared<Tensor>(Array1D<double, 1> {scalingFactor});

    std::shared_ptr<MetaOperator_Op> metaOp = std::static_pointer_cast<MetaOperator_Op>(metaOpNode->getOperator());

    std::shared_ptr<Node> mulNode = getSubNode(metaOp->getMicroGraph(), "Mul");

    if (!mulNode)
        Log::warn(" Invalid PTQ MetaOperator, no Mul node found inside ! ");

    mulNode->input(1).first->getOperator()->setOutput(0, scalingFactorTensor);
}

double getScalingFactor(std::shared_ptr<Node> MetaOpNode)
{
    if (MetaOpNode->type() != "Scaling" && MetaOpNode->type() != "Quantizer") {
        Log::warn(" Cannot get the scaling factor on Node of type {}", MetaOpNode->type());
        return 0;
    }

    std::shared_ptr<MetaOperator_Op> metaOp = std::static_pointer_cast<MetaOperator_Op>(MetaOpNode->getOperator());

    std::shared_ptr<Node> mulNode = getSubNode(metaOp->getMicroGraph(), "Mul");

    if (!mulNode) {
        Log::warn(" Invalid PTQ MetaOperator, no Mul found inside node of type {}", MetaOpNode->type());
        return 0;
    }

    auto scalingFactorTensor = std::static_pointer_cast<OperatorTensor>(mulNode->getOperator())->getInput(1);
    std::shared_ptr<Tensor> fallback;
    const Tensor& localTensor = scalingFactorTensor->refCastFrom(fallback, DataType::Float64, "cpu");

    return localTensor.get<double>(0);
}


void setClipRange(std::shared_ptr<Node> quantizerNode, double min, double max)
{
    if (quantizerNode->type() != "Quantizer") {
        Log::warn(" Cannot set the clipping range on Node of type {}", quantizerNode->type());
        return;
    }

    std::shared_ptr<MetaOperator_Op> metaOp = std::static_pointer_cast<MetaOperator_Op> (quantizerNode->getOperator());

    std::shared_ptr<Node> clipNode = getSubNode(metaOp->getMicroGraph(), "Clip");

    if (!clipNode) {
        Log::warn(" Invalid PTQ MetaOperator, no Clip found inside node of type {}", quantizerNode->type());
        return;
    }

    std::shared_ptr<Clip_Op> clipOp = std::static_pointer_cast<Clip_Op>(clipNode->getOperator());
    clipOp->max() = max;
    clipOp->min() = min;
}
}