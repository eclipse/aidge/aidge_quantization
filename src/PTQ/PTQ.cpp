/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include "aidge/quantization/PTQ/CLE.hpp"
#include "aidge/quantization/PTQ/Clipping.hpp"
#include "aidge/quantization/PTQ/PTQ.hpp"
#include "aidge/quantization/PTQ/PTQMetaOps.hpp"


#include "aidge/data/Tensor.hpp"
#include "aidge/graph/GraphView.hpp"
#include "aidge/graph/Node.hpp"
#include "aidge/scheduler/SequentialScheduler.hpp"
#include "aidge/scheduler/Scheduler.hpp"
#include "aidge/utils/Log.hpp"

#include "aidge/operator/Producer.hpp"
#include "aidge/operator/Mul.hpp"
#include "aidge/operator/ReLU.hpp"
#include "aidge/operator/BatchNorm.hpp"
#include "aidge/operator/Conv.hpp"

#include "aidge/recipes/Recipes.hpp"
#include "aidge/recipes/QuantRecipes.hpp"


namespace Aidge
{

bool isAffine(std::shared_ptr<Node> node)
{
    return (affineNodeTypes.find(node->type()) != affineNodeTypes.end());
}

bool isSeamless(std::shared_ptr<Node> node)
{
    return (seamlessNodeTypes.find(node->type()) != seamlessNodeTypes.end());
}

bool isMerging(std::shared_ptr<Node> node)
{
    return (mergingNodeTypes.find(node->type()) != mergingNodeTypes.end());
}

bool checkArchitecture(std::shared_ptr<GraphView> graphView)
{
    std::set<std::string> otherNodeTypes({"Flatten", "Softmax", "BatchNorm2D", "ReLU", "Producer"});

    for (std::shared_ptr<Node> node : graphView->getNodes())
    {
        bool isOther = otherNodeTypes.find(node->type()) != otherNodeTypes.end();
        if (!isOther && !isAffine(node) && !isSeamless(node) && !isMerging(node)) {
            Log::warn(" GraphView can't be quantized : node type {} is not supported !", node->type());
            return false;
        }
    }

    return true;
}

static void fillTensor(std::shared_ptr<Tensor> tensor, double value)
{
    // Get the tensor data pointer
    double * castedTensor = static_cast <double *> (tensor->getImpl()->rawPtr());

    // Fill the tensor
    for(std::size_t i = 0; i < tensor->size(); i++)
        castedTensor[i] = value;
}

static void rescaleTensor(std::shared_ptr<Tensor> tensor, double scaling)
{
    // Get the tensor data pointer
    double * castedTensor = static_cast <double *> (tensor->getImpl()->rawPtr());

    // Rescale the tensor
    for(std::size_t i = 0; i < tensor->size(); i++)
        castedTensor[i] *= scaling;
}

static void roundTensor(std::shared_ptr<Tensor> tensor)
{
    // Get the tensor data pointer
    double * castedTensor = static_cast <double *> (tensor->getImpl()->rawPtr());

    // Rescale the tensor
    for(std::size_t i = 0; i < tensor->size(); i++)
        castedTensor[i] = std::nearbyint(castedTensor[i]);//Round
}

static double getTensorAbsoluteMax(std::shared_ptr <Tensor> tensor)
{
    // Get the tensor data pointer and edit it
    double * castedTensor = static_cast<double*>(tensor->getImpl()->rawPtr());

    // Get the tensor absolute max value
    double maxValue = 0.0f;
    for(std::size_t i = 0; i < tensor->size(); ++i) {
        if(std::fabs(castedTensor[i]) > maxValue) {
            maxValue = std::fabs(castedTensor[i]);
        }
    }
    return maxValue;
}

// TODO : pass nodeVector by reference ...
static std::vector<std::shared_ptr<Node>> removeMatchingNodes(std::vector<std::shared_ptr<Node>> nodeVector, std::string nodeType)
{
    std::vector<std::shared_ptr<Node>> remainingNodes;
    for (std::shared_ptr<Node> node : nodeVector)
        if (node->type() != nodeType)
            remainingNodes.push_back(node);

    return remainingNodes;
}

static void fixScheduling(std::vector<std::shared_ptr<Node>>& nodeVector) {

    std::vector<std::shared_ptr<Node>> correctedVector;

    for (int i = (nodeVector.size() - 1); i >= 0; --i)
    {
        std::shared_ptr<Node> node = nodeVector[i];
        bool isAlreadyInside = (std::find(correctedVector.begin(), correctedVector.end(), node) != correctedVector.end());
        if (!isAlreadyInside)
            correctedVector.push_back(node);
    }

    std::reverse(correctedVector.begin(), correctedVector.end());

    nodeVector = correctedVector;
}

static std::shared_ptr<Tensor> getWeightTensor(std::shared_ptr<Node> node)
{
    return std::static_pointer_cast<OperatorTensor>(node->getOperator())->getInput(1);
}

static std::shared_ptr<Tensor> getBiasTensor(std::shared_ptr<Node> node)
{
    return std::static_pointer_cast<OperatorTensor>(node->getOperator())->getInput(2);
}

std::vector<std::shared_ptr<Node>> retrieveNodeVector(std::shared_ptr<GraphView> graphView, bool newSchedule, bool verbose)
{
    std::vector<std::shared_ptr<Node>> nodeVector;

    SequentialScheduler scheduler(graphView);

    if (newSchedule)
    {
        scheduler.resetScheduling();
        scheduler.generateScheduling(); // old way : scheduler.forward(); 
    }

    nodeVector = scheduler.getStaticScheduling();

    fixScheduling(nodeVector);
    nodeVector = removeMatchingNodes(nodeVector, "Producer");

    if (verbose) 
    {
        Log::info("NB OF NODES = {}", nodeVector.size());
        for (std::shared_ptr<Node> node : nodeVector)
            Log::info("{} {}", node->type(), node->name());
    }

    return nodeVector;    
}

static std::shared_ptr<Node> getFirstNode(std::shared_ptr<GraphView> graphView)
{
    return retrieveNodeVector(graphView)[0];
}

void prepareNetwork(std::shared_ptr<GraphView> graphView)
{
    removeFlatten(graphView);

    bool containsBatchNorm = false;
    std::vector<std::shared_ptr<Node>> nodeVector = retrieveNodeVector(graphView);

    for (std::shared_ptr<Node> node : nodeVector)
        if (node->type() == "BatchNorm")
        {
            containsBatchNorm = true;
            break;
        }

    if (containsBatchNorm)
        fuseBatchNorm(graphView);   

    popSoftMax(graphView);
}

// TODO : enhance this by modifying OperatorImpl in "core" ...
static DataType getDataType(std::shared_ptr<Node> node)
{
    auto op = std::static_pointer_cast<OperatorTensor>(node->getOperator());
    return op->getOutput(0)->dataType();
}

// XXX HERE : Branches containing only Seamless nodes should be considered as residual too !!!
void insertResidualNodes(std::shared_ptr<GraphView> graphView)
{
    // TODO: double check this ...

    std::vector<std::shared_ptr<Node>> nodeVector = retrieveNodeVector(graphView);

    for (std::shared_ptr<Node> node : nodeVector)
    {
        if (isMerging(node))
        {
            int nbParents = node->getParents().size();
            for (int i = 0; i < nbParents; i++)
            {
                std::shared_ptr<Node> parentNode = node->getParent(i);
                bool parentIsForking = (parentNode->getChildren().size() > 1);

                if (parentIsForking)
                {
                    // temporary verbose ...
                    Log::info(" ### found residual branch at index {}", i);
                    Log::info(" ### inserting multiplicative node ...");

                    std::string residualNodeName = makeUniqueName(parentNode->name() + "_Res", graphView);
                    std::shared_ptr<Node> residualNode = Scaling(1.0, residualNodeName);

                    residualNode->getOperator()->setDataType(DataType::Float64); //getDataType(parentNode)
                    residualNode->getOperator()->setBackend("cpu");

                    graphView->insertParent(node, residualNode, i, 0, 0);
                }
            }
        }
    }
}

static int getInputIndex(std::shared_ptr<Node> node, std::shared_ptr<Node> parentNode)
{
    int index = 0;
    while (node->getParent(index) != parentNode) 
        index++;
    return index;
}

void insertScalingNodes(std::shared_ptr<GraphView> graphView)
{
    insertResidualNodes(graphView);

    std::set<std::shared_ptr<Node>> nodeSet = graphView->getNodes();

    for (std::shared_ptr<Node> parentNode : nodeSet)
    {
        if (isAffine(parentNode) || isMerging(parentNode))
        {
            std::string scalingNodeName = makeUniqueName(parentNode->name() + "_Scaling", graphView);
            std::shared_ptr<Node> scalingNode = Scaling(1.0, scalingNodeName);

            scalingNode->getOperator()->setDataType(DataType::Float64); // getDataType(parentNode)
            scalingNode->getOperator()->setBackend("cpu");

            if (parentNode->getChildren().size() > 0)
            {
                // SCALING NODE INSERTION
                
                // We always have one output from Affine and Add nodes, but possibly multiple childs
                std::vector<std::shared_ptr<Node>> nextNodes = parentNode->getChildren(0); 

                // For each node in nextNodes store the connexion index
                std::vector<int> inputIndices(nextNodes.size());
                for (std::size_t i = 0; i < nextNodes.size(); i++)
                    inputIndices[i] = getInputIndex(nextNodes[i], parentNode);
                    
                for (std::shared_ptr<Node> nextNode : nextNodes)
                    parentNode->removeChild(nextNode, 0);

                parentNode->addChild(scalingNode, 0, 0);

                for (std::size_t i = 0; i < nextNodes.size(); i++)
                    scalingNode->addChild(nextNodes[i], 0, inputIndices[i]);

                graphView->add(scalingNode);
            }
            else
            {
                // Log::info(" last node reached ! ");
                parentNode->addChild(scalingNode, 0, 0);
                graphView->add(scalingNode);
            }
        }
    }
}

static std::shared_ptr<Node> getPreviousScalingNode(std::shared_ptr<Node> mergingNode)
{
    std::shared_ptr<Node> currNode = mergingNode;
    while(currNode->type() != "Scaling")
    {
        if (currNode->getParents().size() == 0)
        {
            Log::warn(" Warning : No previous Scaling node were found ! ");
            break;
        }
        currNode = currNode->getParents()[0];
    }
    return currNode;
}

// XXX double check this !
static bool nodeHasBias(std::shared_ptr<Node> node)
{
    if (node->getParents().size() == 3)
    {
        std::shared_ptr<Tensor> biasTensor = getBiasTensor(node);
        if (biasTensor)
            return true;
    }
    return false;
}

void normalizeParameters(std::shared_ptr<GraphView> graphView)
{
    // CREATE THE ACCUMULATED RATIO MAP ///////////////////////////////////////

    std::vector<std::shared_ptr<Node>> nodeVector = retrieveNodeVector(graphView);

    std::map<std::string, double> accumulatedRatios;
    for (std::shared_ptr<Node> node : nodeVector)
    {
        accumulatedRatios.insert(std::make_pair(node->name(), 1.0));
    }

    // ITERATE OVER THE GRAPH /////////////////////////////////////////////////

    std::shared_ptr<Node> firstNode = getFirstNode(graphView);

    for (std::shared_ptr<Node> node : nodeVector)
    {
        // Scaling nodes still have a ratio of 1, so they are seamless ...
        if (node->type() == "ReLU" || node->type() == "Scaling" || isSeamless(node))
        {
            if (node != firstNode)
            {
                std::shared_ptr<Node> prevNode = node->getParent(0);
                accumulatedRatios[node->name()] = accumulatedRatios[prevNode->name()];
            }
        }

        // Residual nodes should enter in this category but their ratio is 1 ...
        if (isAffine(node))
        {
            // Rescale the weight tensor
            std::shared_ptr<Tensor> weightTensor = getWeightTensor(node);
            double scaling = getTensorAbsoluteMax(weightTensor);
            double ratio = 1.0 / scaling;
            rescaleTensor(weightTensor, ratio);

            // Accumulate the ratio
            if (node == firstNode)
            {
                accumulatedRatios[node->name()] = ratio;
            }
            else
            {
                std::shared_ptr<Node> prevNode = node->getParent(0);
                accumulatedRatios[node->name()] = accumulatedRatios[prevNode->name()] * ratio;
            }

            // Handle the bias ..

            if (nodeHasBias(node))
            {
                std::shared_ptr<Tensor> biasTensor = getBiasTensor(node);
                rescaleTensor(biasTensor, accumulatedRatios[node->name()] );
            }
        }

        if (isMerging(node))
        {
            std::vector<std::shared_ptr<Node>> mergingNodes = node->getParents();

            // Compute the max ratio ...
            double maxRatio = 0;
            for (std::shared_ptr<Node> mergingNode : mergingNodes)
            {
                double merginNodeRatio = accumulatedRatios[mergingNode->name()];
                if (merginNodeRatio > maxRatio)
                    maxRatio = merginNodeRatio;
            }

            accumulatedRatios[node->name()] = maxRatio;

            // Rescale the previous scaling Nodes
            for (std::shared_ptr<Node> mergingNode : mergingNodes)
            {
                double mergingNodeRatio = accumulatedRatios[mergingNode->name()];
                double rescaling = mergingNodeRatio / maxRatio;

                std::shared_ptr<Node> scalingNode = getPreviousScalingNode(mergingNode);

                double currScalingFactor = getScalingFactor(scalingNode);
                updateScalingFactor(scalingNode, currScalingFactor / rescaling);

                accumulatedRatios[mergingNode->name()] /= rescaling; // optional ...
            }
        }
    }
}

// XXX TODO : take care of the CUDA backend for this too !!!
std::map<std::string, double> computeRanges(std::shared_ptr<GraphView> graphView, std::shared_ptr<Tensor> inputTensor, bool scalingNodesOnly)
{
    std::map<std::string, double> valueRanges;

    SequentialScheduler scheduler(graphView);
    scheduler.resetScheduling();

    // Inference ... 

    scheduler.forward(true, {inputTensor});

    // Gather ranges ...

    std::set<std::shared_ptr<Node>> nodeSet = graphView->getNodes();
    for (std::shared_ptr<Node> node : nodeSet)
    {
        if ((scalingNodesOnly && (node->type() == "Scaling")) || (!scalingNodesOnly && (node->type() != "Producer")))
        {
            std::shared_ptr<Operator> nodeOperator = node->getOperator();
            std::shared_ptr<Tensor> valueTensor = std::static_pointer_cast<Tensor> (nodeOperator->getRawOutput(0));
            double range = getTensorAbsoluteMax(valueTensor);

            // Associate the value to the scaling node ...
            valueRanges.insert(std::make_pair(node->name(), range));
        }
    }

    return valueRanges;
}

std::map<std::string, double> computeRanges(std::shared_ptr<GraphView> graphView, std::vector<std::shared_ptr<Tensor>> inputDataSet, bool scalingNodesOnly, bool useCuda)
{
    std::map<std::string, double> valueRanges;
    std::set<std::shared_ptr<Node>> nodeSet = graphView->getNodes();
    
    // std::shared_ptr<Node> inputNode = getFirstNode(graphView);

    for (std::shared_ptr<Node> node : nodeSet)
        if ((scalingNodesOnly && (node->type() == "Scaling")) || (!scalingNodesOnly && (node->type() != "Producer")))
            valueRanges.insert(std::make_pair(node->name(), 0));

    if (useCuda)
        graphView->setBackend("cuda");

    SequentialScheduler scheduler(graphView);
    scheduler.resetScheduling();

    int it = 0;

    for (std::shared_ptr<Tensor> sample : inputDataSet)
    {
        //Log::info(" IT : {}", it++);

        // Inference ...

        if (useCuda)
            sample->setBackend("cuda");

        scheduler.forward(true, {sample});

        // Gather the sample ranges ...

        std::map<std::string, double> sampleRanges;
        for (std::shared_ptr<Node> node : nodeSet)
        {
            if ((scalingNodesOnly && (node->type() == "Scaling")) || (!scalingNodesOnly && (node->type() != "Producer")))
            {
                std::shared_ptr<Operator> nodeOperator = node->getOperator();
                std::shared_ptr<Tensor> valueTensor = std::static_pointer_cast<Tensor> (nodeOperator->getRawOutput(0));
                
                if (useCuda)
                    valueTensor->setBackend("cpu");

                double range = getTensorAbsoluteMax(valueTensor);

                // Associate the value to the scaling node ...
                sampleRanges.insert(std::make_pair(node->name(), range));

                if (useCuda)
                    valueTensor->setBackend("cuda");
            }
        }

        // Update the global value ranges ...

        for (std::shared_ptr<Node> node : nodeSet)
        {
            if ((scalingNodesOnly && (node->type() == "Scaling")) || (!scalingNodesOnly && (node->type() != "Producer")))
                {
                    std::string nodeName = node->name();
                    if (sampleRanges[nodeName] > valueRanges[nodeName])
                        valueRanges[nodeName] = sampleRanges[nodeName];
                }
        }

        if (useCuda)
            sample->setBackend("cpu");
    }   

    if (useCuda)
        graphView->setBackend("cpu");

    return valueRanges;
}

void normalizeActivations(std::shared_ptr<GraphView> graphView, std::map<std::string, double> valueRanges)
{
    std::shared_ptr<Node> firstNode = getFirstNode(graphView);

    // CREATE THE SCALING FACTOR MAP //////////////////////////////////////////

    std::vector<std::shared_ptr<Node>> nodeVector = retrieveNodeVector(graphView);

    std::map<std::string, double> scalingFactors;

    for (std::shared_ptr<Node> node : nodeVector)
        scalingFactors.insert(std::make_pair(node->name(), 1.0));

    // ITERATE OVER THE GRAPH /////////////////////////////////////////////////

    for (std::shared_ptr<Node> node : nodeVector)
    {
        // Seamless scaling factor propagation ...
    
        if (isAffine(node) || isSeamless(node) || node->type() == "ReLU") 
        {
            if (node == firstNode)
            {
                scalingFactors[node->name()] = 1.0;
            }
            else
            {
                std::shared_ptr<Node> prevNode = node->getParent(0);
                scalingFactors[node->name()] = scalingFactors[prevNode->name()];
            }
        }

        // Here prevNode is either a 'Affine' or a 'Merging'
        // => do not split the cases, just handle the bias ...

        if (node->type() == "Scaling") 
        {
            // retrieve the previous scaling factor ...
            std::shared_ptr<Node> prevNode = node->getParent(0);
            double prevScalingFactor = scalingFactors[prevNode->name()];

            // ValueRanges must contains all the scaling nodes !!!
            double scalingFactor = valueRanges[node->name()]; 

            double currScalingFactor = getScalingFactor(node);
            updateScalingFactor(node, currScalingFactor / (scalingFactor / prevScalingFactor));

            scalingFactors[node->name()] = scalingFactor;

            // If prevNode is Affine, fix the bias ...

            if (isAffine(prevNode))
            {
                bool prevNodeHasBias = nodeHasBias(prevNode);
                if (prevNodeHasBias)  
                {
                    std::shared_ptr<Tensor> biasTensor = getBiasTensor(prevNode);
                    rescaleTensor(biasTensor, 1.0 / prevScalingFactor);
                }
            }
        }

        // Merging nodes handling : use a maximum arbritation ...
        
        if (isMerging(node))
        {
            std::vector<std::shared_ptr<Node>> mergingNodes = node->getParents();

            // Compute the max scaling ...
            double maxScaling = 0;
            for (std::size_t i = 0; i < mergingNodes.size(); i++)
            {
                double merginNodeScaling = scalingFactors[mergingNodes[i]->name()];
                if (merginNodeScaling > maxScaling) {
                    maxScaling = merginNodeScaling;
                }
            }

            scalingFactors[node->name()] = maxScaling;

            for (std::shared_ptr<Node> mergingNode : mergingNodes)
            {
                double mergingNodeScaling = scalingFactors[mergingNode->name()];
                double rescaling = mergingNodeScaling / maxScaling;

                std::shared_ptr<Node> scalingNode = getPreviousScalingNode(mergingNode);
                //Log::info(" SCALING NODE : {} {}", scalingNode->type(), scalingNode->name());

                double currScalingFactor = getScalingFactor(scalingNode);
                updateScalingFactor(scalingNode, currScalingFactor * rescaling);                
            }
        }
    }
}

std::map<std::string, std::pair<bool, bool>> computeSignMap(std::shared_ptr<GraphView> graphView, bool verbose)
{
    std::shared_ptr<Node> firstNode = getFirstNode(graphView);

    std::map<std::string, std::pair<bool, bool>> signMap;

    std::pair<bool, bool> unsignedPair(true, true);
    for (std::shared_ptr<Node> node : graphView->getNodes())
        if (node->type() != "Producer")
            signMap.insert(std::make_pair(node->name(), unsignedPair));

    // ITERATE OVER THE GRAPH

    std::vector<std::shared_ptr<Node>> nodeVector = retrieveNodeVector(graphView);

    for (std::shared_ptr<Node> node : nodeVector)
    {
        bool isFirstNode = (node == firstNode);

        if (isAffine(node))
        {   
            // Affine nodes always have a single parent 
            if (!isFirstNode)
                signMap[node->name()].first = signMap[node->getParent(0)->name()].second;
            else
                signMap[node->name()].first = false;

            signMap[node->name()].second = false;
        } 

        if (node->type() == "Scaling") 
        {
            signMap[node->name()].second = false;

            // Scaling nodes always have a single parent 
            std::shared_ptr<Node> parent = node->getParent(0); 

            bool allChildrenAreReLU = true;
            allChildrenAreReLU &= !(node->getChildren().empty()); // a bit convoluted ...
            for (std::shared_ptr <Node> child : node->getChildren())
                allChildrenAreReLU &= (child->type() == "ReLU");

            // Correct the previous single node (when it is an Affine node) ...
            if (allChildrenAreReLU)
                if (isAffine(parent) || isMerging(parent))
                    signMap[parent->name()].second = true;

            // Maintain unsigned output
            if (signMap[parent->name()].second)
                signMap[node->name()].second = true;

            // Set the link ...
            signMap[node->name()].first = signMap[parent->name()].second;
        }
       
        if (isMerging(node))
        {
            std::vector<std::shared_ptr<Node>> parentNodes = node->getParents(); 

            bool allParentAreSigned = true;
            bool allParentAreUnsigned = true;
            for(std::shared_ptr<Node> parent : parentNodes)
            {
                bool parentSign = signMap[parent->name()].second;
                allParentAreSigned &= !parentSign;
                allParentAreUnsigned &= parentSign;
            }

            if (allParentAreSigned)
                signMap[node->name()] = std::make_pair(false, false);
            else if (allParentAreUnsigned)
                signMap[node->name()] = std::make_pair(true, true);
            else 
            {
                // Arbitration : Signed type wins !
                for(std::shared_ptr<Node> parent : parentNodes)
                {
                    while (parent->type() != "Scaling")
                    {
                        signMap[parent->name()] = std::make_pair(false, false);
                        // We are on a branch so nodes always have 1 parent ...
                        parent = parent->getParent(0); 
                    }

                    signMap[parent->name()].second = false;
                }

                signMap[node->name()].first = false;
            }
        }
        
        if (node->type() == "ReLU" || isSeamless(node))
        {
            // Thoses nodes always have a single parent 
            std::shared_ptr<Node> parent = node->getParent(0); 
            if (parent)
            {
                signMap[node->name()].first = signMap[parent->name()].second;
                signMap[node->name()].second = signMap[node->name()].first;
            }
            
        }
    }

    // VERBOSE

    if (verbose)
    {
        Log::info(" === SIGN MAP === ");
        for (std::shared_ptr<Node> node : nodeVector)
            Log::info(" {}{} | {}", static_cast<int>(signMap[node->name()].first), static_cast<int>(signMap[node->name()].second), node->name());
    }

    // SANITY CHECK (TEMPORARY)

    for (std::shared_ptr<Node> node : nodeVector)
    {
        for (std::shared_ptr<Node> child : node->getChildren()) 
        {
            if (signMap[node->name()].second != signMap[child->name()].first)
                Log::error(" computeSignMap : link is not sane ! ({} -> {})", node->name(), child->name());                    
        }
    }
    
    return signMap;
}


void quantizeNormalizedNetwork(std::shared_ptr<GraphView> graphView, std::uint8_t nbBits, bool noQuant, bool optimizeSigns, bool verbose)
{
    if (optimizeSigns && noQuant)
    {
        AIDGE_THROW_OR_ABORT(std::runtime_error,"Signs optimization can not be applied if network is not fully quantized ...");
    }

    double signedMax = (1 << (nbBits - 1)) - 1;
    double unsignedMax = (1 << nbBits) - 1;

    std::map<std::string, std::pair<bool, bool>> signMap;

    if (optimizeSigns)
        signMap = computeSignMap(graphView, verbose);
    else
    {
        std::pair<bool, bool> signedPair(false, false);
        for (std::shared_ptr<Node> node : graphView->getNodes())
            if (node->type() != "Producer")
                signMap.insert(std::make_pair(node->name(), signedPair));
    }

    // ITERATE OVER THE GRAPH /////////////////////////////////////////////////

    std::vector<std::shared_ptr<Node>> nodeVector = retrieveNodeVector(graphView);

    for (std::shared_ptr<Node> node : nodeVector)
    {
        if (isAffine(node))
        {
            // Rescale the weight tensor

            std::shared_ptr<Tensor> weightTensor = getWeightTensor(node);
            rescaleTensor(weightTensor, signedMax);

            if (!noQuant)
                roundTensor(weightTensor);

            // Rescale the bias tensor

            if (nodeHasBias(node))  
            {
                bool inputIsUnsigned = signMap[node->name()].first;
                double rescaling = inputIsUnsigned ? unsignedMax * signedMax : signedMax * signedMax;
                

                std::shared_ptr<Tensor> biasTensor = getBiasTensor(node);
                rescaleTensor(biasTensor, rescaling);

                if (!noQuant)
                    roundTensor(biasTensor);
            }

            // Compensate the rescaling using the next Scaling node

            double rescaling = 1.0 / signedMax;

            bool inputIsUnsigned  = signMap[node->name()].first; 
            bool outputIsUnsigned = signMap[node->name()].second;

            rescaling /= inputIsUnsigned  ? unsignedMax : signedMax;
            rescaling *= outputIsUnsigned ? unsignedMax : signedMax;
            
            std::shared_ptr<Node> scalingNode = *(node->getChildren().begin()); // Assert if scalingNode is a Scaling ...

            double currScalingFactor = getScalingFactor(scalingNode);
            updateScalingFactor(scalingNode, currScalingFactor * rescaling);
        }
        
        if (isMerging(node))
        {
            double rescaling = 1.0;

            bool inputIsUnsigned  = signMap[node->name()].first;
            bool outputIsUnsigned = signMap[node->name()].second;

            rescaling /= inputIsUnsigned  ? unsignedMax : signedMax;
            rescaling *= outputIsUnsigned ? unsignedMax : signedMax;

            std::shared_ptr<Node> scalingNode = *(node->getChildren().begin()); // Assert if scalingNode is a Scaling ...
        
            double currScalingFactor = getScalingFactor(scalingNode); // XXX bad naming
            updateScalingFactor(scalingNode, currScalingFactor * rescaling);
        }
        
        // Handle the Scaling Nodes ...

        if (node->type() == "Scaling")
        {
            if (!noQuant) 
            {  
                // Replace  the  Scaling Node by Quantizer

                std::shared_ptr<Node> quantizerNode = Quantizer(getScalingFactor(node), -(signedMax + 1), signedMax, node->name());
                quantizerNode->getOperator()->setDataType(DataType::Float64); // getDataType(parentNode)
                quantizerNode->getOperator()->setBackend("cpu");

                graphView->replace({node}, {quantizerNode});

                if (optimizeSigns)
                {
                    double rescaling = 1.0;

                    bool inputIsUnsigned  = signMap[node->name()].first;
                    bool outputIsUnsigned = signMap[node->name()].second;

                    rescaling /= inputIsUnsigned  ? unsignedMax : signedMax;
                    rescaling *= outputIsUnsigned ? unsignedMax : signedMax;

                    double currScalingFactor = getScalingFactor(quantizerNode);
                    updateScalingFactor(quantizerNode, currScalingFactor * rescaling);

                    if(outputIsUnsigned)
                    {
                        setClipRange(quantizerNode,0,unsignedMax);
                    }                 
                }
            }
        }
    }
}

static void insertCompensationNodes(std::shared_ptr<GraphView> graphView, std::uint8_t nbBits)
{
    // XXX Use the signMap to increase the resolution when possible ...
    double signedMax = (1 << (nbBits - 1)) - 1;    

    std::vector<std::shared_ptr<Node>> nodeVector = retrieveNodeVector(graphView);

    for (std::shared_ptr<Node> node : nodeVector)
    {
        // A merging node is always followed by a scaling node at this point ...

        if (node->type() == "Quantizer")
        {   
            bool prevNodeIsForking = ((node->getParent(0))->getChildren().size() > 1);
            bool prevNodeIsAffine = isAffine(node->getParent(0));
            bool insertNode = prevNodeIsForking || !prevNodeIsAffine;

            if (insertNode)
            {
                // create and insert the multplicative node

                std::string mulNodeName = makeUniqueName(node->name() + "_Mul", graphView);
                std::shared_ptr<Node> mulNode = Mul(mulNodeName);

                mulNode->getOperator()->setDataType(DataType::Float64); // getDataType(parentNode)
                mulNode->getOperator()->setBackend("cpu");

                graphView->insertParent(node, mulNode, 0, 0, 0);

                // create and insert the producer node

                std::shared_ptr<Tensor> inputTensor = std::static_pointer_cast<Tensor> (mulNode->getOperator()->getRawInput(0));
                std::shared_ptr<Tensor> coeffTensor = std::make_shared<Tensor>();

                coeffTensor->setDataType(DataType::Float64); // getDataType(parentNode)
                coeffTensor->setBackend("cpu"); 

                coeffTensor->resize(inputTensor->dims());
                fillTensor(coeffTensor, 1); 

                std::shared_ptr<Node> producerNode = Producer(coeffTensor, makeUniqueName("coeff", graphView));
                producerNode->addChild(mulNode);
                graphView->add(producerNode);

                // rescale the coeffs and edit scaling factor

                fillTensor(coeffTensor, signedMax);

                double currScalingFactor = getScalingFactor(node); // XXX bad naming !
                updateScalingFactor(node, currScalingFactor / signedMax);

                // TODO : double check this !!!
                //std::cout << getTensorAbsoluteMax(coeffTensor) << std::endl;
            }
        }
    }
}

void performSingleShiftApproximation(std::shared_ptr<GraphView> graphView, bool noQuant)
{
    std::vector<std::shared_ptr<Node>> nodeVector = retrieveNodeVector(graphView);

    for (std::shared_ptr<Node> node : nodeVector)
    {
        // Use A meatoperator of type Scaling of MulCompensation instead
        if (isAffine(node) || (node->type() == "Mul"))
        {
            std::shared_ptr<Node> scalingNode = (*node->getChildren().begin());

            double base = getScalingFactor(scalingNode);

            double approx = std::pow(2, std::ceil(std::log2(base)));

            updateScalingFactor(scalingNode,approx);

            double ratio = base / approx;

            std::shared_ptr<Tensor> weightTensor = getWeightTensor(node);
            rescaleTensor(weightTensor, ratio);
            if (!noQuant)
                roundTensor(weightTensor);

            if (nodeHasBias(node))
            {
                std::shared_ptr<Tensor> biasTensor = getBiasTensor(node);
                rescaleTensor(biasTensor, ratio);
                if (!noQuant)
                roundTensor(biasTensor);
            }
        }
    }
}

static void printScalingFactors(std::shared_ptr<GraphView> graphView)
{
    Log::info(" === SCALING FACTORS === ");
    for (auto node : retrieveNodeVector(graphView))
        if (node->type() == "Scaling" || node->type() == "Quantizer")
        {
            double scalingFactor = getScalingFactor(node);
            Log::info(" {:.6f} ({})", scalingFactor, node->name());
        }
}

static void setupDataType(std::shared_ptr<GraphView> graphView, std::vector<std::shared_ptr<Tensor>> inputDataSet, DataType dataType)
{
    graphView->setDataType(dataType);

    for (auto inputNode : graphView->inputNodes()) {
        auto op = std::static_pointer_cast<OperatorTensor>(inputNode->getOperator());
        auto inputTensor = op->getInput(0);
        if (inputTensor)
            inputTensor->setDataType(dataType);
    }

    for (auto tensor : inputDataSet)
        tensor->setDataType(dataType);
}

static void printRanges(std::shared_ptr<GraphView> graphView, std::map<std::string, double> valueRanges)
{
    SequentialScheduler scheduler(graphView);
    scheduler.resetScheduling();
    scheduler.generateScheduling();

    auto scheduling = scheduler.getStaticScheduling();
    for (auto node : scheduling)
        if (node->type() == "Scaling")
            fmt::println("{} range = {}", node->name(), valueRanges[node->name()]);
}

void quantizeNetwork(std::shared_ptr<GraphView> graphView, std::uint8_t nbBits, std::vector<std::shared_ptr<Tensor>> inputDataSet, Clipping clippingMode, bool noQuant, bool optimizeSigns, bool singleShift, bool useCuda, bool verbose)
{
    Log::info(" === QUANT PTQ 0.2.21 === ");

    graphView->setBackend("cpu");

    DataType initialDataType = (inputDataSet[0])->dataType();
    setupDataType(graphView, inputDataSet, DataType::Float64);

    if (!checkArchitecture(graphView))
        return;

    Log::info(" Preparing the network for the PTQ ... ");
    prepareNetwork(graphView);

    Log::info(" Inserting the scaling nodes ...");
    insertScalingNodes(graphView);

    crossLayerEqualization(graphView);

    Log::info(" Normalizing the parameters ...");
    normalizeParameters(graphView);

    Log::info(" Computing the value ranges ...");
    std::map<std::string, double> valueRanges = computeRanges(graphView, inputDataSet, true, useCuda);

    //std::cout << " === RANGES (BEFORE ADJUST) ===" << std::endl;
    //printRanges(graphView, valueRanges);

    Log::info(" Optimizing the clipping values ...");
    valueRanges = adjustRanges(clippingMode, valueRanges, nbBits, graphView, inputDataSet, useCuda, verbose);

    //std::cout << " === RANGES (AFTER ADJUST) ===" << std::endl;
    //printRanges(graphView, valueRanges);

    Log::info(" Normalizing the activations ...");
    normalizeActivations(graphView, valueRanges);

    Log::info(" Quantizing the normalized network ...");
    quantizeNormalizedNetwork(graphView, nbBits, noQuant, optimizeSigns, verbose);

    if (singleShift)
    {
        Log::info( " Inserting the compensation nodes ...");
        insertCompensationNodes(graphView, nbBits);

        Log::info(" Performing the Single-Shift approximation ...");
        performSingleShiftApproximation(graphView, noQuant);
    }

    if (verbose)
        printScalingFactors(graphView);

    //std::cout << " === SCALINGS (BEFORE CAST) ===" << std::endl;
    //printScalingFactors(graphView);

    setupDataType(graphView, inputDataSet, initialDataType);
    if (useCuda)
        graphView->setBackend("cuda");

    //std::cout << " === SCALINGS (AFTER CAST) ===" << std::endl;
    //printScalingFactors(graphView);

    Log::info(" Reseting the scheduler ...");
    SequentialScheduler scheduler(graphView);
    scheduler.resetScheduling();

    Log::info(" Network is quantized !");
}

std::map<std::string, double> getWeightRanges(std::shared_ptr<GraphView> graphView)
{
    std::map<std::string, double> weightRanges;

    for (std::shared_ptr<Node> node : graphView->getNodes())
    {
        if (isAffine(node))
        {
            std::shared_ptr<Tensor> weightTensor = getWeightTensor(node);
            double range = getTensorAbsoluteMax(weightTensor);
            weightRanges.insert(std::make_pair(node->name(), range));
        }
    }

    return weightRanges;
}

void clearBiases(std::shared_ptr<GraphView> graphView) 
{
    for (std::shared_ptr<Node> node : graphView->getNodes()) {
        if (node->type() == "FC" || node->type() == "Conv2D") {
            std::shared_ptr<Tensor> biasTensor = std::static_pointer_cast<OperatorTensor>(node->getOperator())->getInput(2);
            rescaleTensor(biasTensor, 0);
        }
    }
}

void devPTQ(std::shared_ptr<GraphView> graphView) 
{
    for (std::shared_ptr<Node> node : graphView->getNodes())
        fmt::println(" UUU : {}", node->name());
}

}
