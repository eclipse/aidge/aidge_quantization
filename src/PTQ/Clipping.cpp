/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include "aidge/quantization/PTQ/CLE.hpp"
#include "aidge/quantization/PTQ/Clipping.hpp"
#include "aidge/quantization/PTQ/PTQ.hpp"

#include "aidge/scheduler/SequentialScheduler.hpp"
#include "aidge/scheduler/Scheduler.hpp"

namespace Aidge
{
    
std::map<std::string, std::vector<int>> computeHistograms(std::map<std::string, double> valueRanges, int nbBins, std::shared_ptr<GraphView> graphView, std::vector<std::shared_ptr<Tensor>> inputDataSet, bool useCuda)
{
    if (useCuda)
        graphView->setBackend("cuda");

    std::shared_ptr<Node> firstNode = retrieveNodeVector(graphView)[0];

    //std::cout << " COMPUTING HISTOGRAMS ... " << std::endl;

    std::map<std::string, std::vector<int>> histograms;

    SequentialScheduler scheduler(graphView);
    scheduler.resetScheduling();

    // Setup the histograms ...

    for (std::shared_ptr<Node> node : graphView->getNodes())
    {
        bool isInsideRanges = (valueRanges.find(node->name()) != valueRanges.end());
        if (isInsideRanges)
        {
            std::vector<int> histogram;
            for (int i = 0; i < nbBins; i++)
                histogram.push_back(0);

            histograms.insert(std::make_pair(node->name(), histogram));
        }
    }

    // Fill the histograms ...

    scheduler.resetScheduling();

    int it = 0;

    for (std::shared_ptr<Tensor> inputTensor : inputDataSet)
    {
        Log::debug(" IT (BIS) : {}", it++);

        // Inference ...

        if (useCuda)
            inputTensor->setBackend("cuda");

        scheduler.forward(true, {inputTensor});

        // Gather values ...

        for (std::shared_ptr<Node> node : graphView->getNodes())
        {
            bool isInsideRanges = (valueRanges.find(node->name()) != valueRanges.end());
            if (isInsideRanges)
            {
                double valueRange = valueRanges[node->name()];

                std::shared_ptr<Operator> nodeOperator = node->getOperator();
                std::shared_ptr<Tensor> valueTensor = std::static_pointer_cast<Tensor> (nodeOperator->getRawOutput(0));

                if (useCuda)
                    valueTensor->setBackend("cpu");

                double * castedTensor = static_cast<double *> (valueTensor->getImpl()->rawPtr());

                std::vector<int> nodeHistogram = histograms[node->name()];
                for(std::size_t i = 0; i < valueTensor->size(); i++)
                {
                    std::size_t bin = std::round(std::abs(castedTensor[i] / valueRange * nbBins));
                    bin = std::min(bin, nodeHistogram.size() - 1);
                    nodeHistogram[bin]++;
                }

                histograms[node->name()] = nodeHistogram;   

                if (useCuda)
                    valueTensor->setBackend("cuda");
            }
        }

        if (useCuda)
            inputTensor->setBackend("cpu");
    }

    if (useCuda)
        graphView->setBackend("cpu");

    return histograms;
}

double computeMEClipping(std::vector<int> histogram, std::uint8_t nbBits, double exponent)
{
    int nbBins = histogram.size();
    int nbIter = 100;
    int signedMax = (1 << (nbBits - 1)) - 1;

    std::vector<double> clippingErrors;
    for (int it = 1; it < nbIter; it++)
    {
        // Compute the rounding cost of this particular clipping ...
        double accumulatedError = 0.0;
        double clipping = it / static_cast<double> (nbIter);
        for (int bin = 0; bin < nbBins; bin++)
        {
            double value = (bin + 0.5) / nbBins;
            double scaling = signedMax / clipping;
            double rounded = std::round(value * scaling) / scaling;
            double clipped = std::min(clipping, rounded);

            double approxError = std::abs(clipped - value);
            accumulatedError += std::pow(approxError, exponent) * histogram[bin];
        }
        clippingErrors.push_back(accumulatedError);
    }

    std::vector<double>::iterator it = std::min_element(clippingErrors.begin() + 1, clippingErrors.end()); 
    int bestBin = static_cast<int> (std::distance(clippingErrors.begin(), it)) + 1;
    double bestClipping = static_cast<double> (bestBin) / static_cast<double> (nbIter);
    
    return bestClipping;
}

double computeKLClipping(std::vector<int> refHistogram, std::uint8_t nbBits)
{
    // KL Clipping

    int nbIter = 100;
    int signedMax = (1 << (nbBits - 1)) - 1;

    double refNorm = 0;
    for (int n : refHistogram)
        refNorm += static_cast<double> (n);

    std::vector<double> clippingErrors;
    for (int it = 1; it < nbIter; it++)
    {
        double clipping = it / static_cast<double> (nbIter);

        // Create the histogram for this particular clipping ...

        std::vector<int> quantHistogram;
        for (int i = 0; i < signedMax; i++)
            quantHistogram.push_back(0);
        
        for (std::size_t refBin = 0; refBin < refHistogram.size(); refBin++)
        {
            double value = (static_cast<double> (refBin) + 0.5f) / static_cast<double> (refHistogram.size());
            int quantBin = std::floor(value / clipping * signedMax);
            quantBin = std::min(quantBin, signedMax-1);
            quantHistogram[quantBin] += refHistogram[refBin];
        }
        
        // Compute the mass of the histogram

        double quantNorm = 0;
        for (std::size_t refBin = 0; refBin < refHistogram.size(); refBin++)
        {
            double value = (static_cast<double> (refBin) + 0.5f) / static_cast<double> (refHistogram.size());
            int quantBin = std::floor(value / clipping * signedMax);
            if (quantBin < static_cast<int> (quantHistogram.size()))
                quantNorm += quantHistogram[quantBin];
        }
        
        // Compute the KL divergence
        
        double accumulatedError = 0.0;
        for (std::size_t refBin = 0; refBin < refHistogram.size(); refBin++)
        {
            double value = (static_cast<double> (refBin) + 0.5f) / static_cast<double> (refHistogram.size());
            int quantBin = std::floor(value / clipping * signedMax);

            double p = static_cast<double> (refHistogram[refBin]) / refNorm;
            double q = (quantBin < static_cast<int> (quantHistogram.size())) ?
                static_cast<double> (quantHistogram[quantBin]) / quantNorm : 0; 

            if (p != 0 && q != 0)
                accumulatedError += q * std::log(q / p);
        }

        clippingErrors.push_back(accumulatedError);
    }

    std::vector<double>::iterator it = std::min_element(clippingErrors.begin() + 1, clippingErrors.end());
    int bestBin = static_cast<int> (std::distance(clippingErrors.begin(), it)) + 1;
    double bestClipping = (static_cast<double> (bestBin)) / static_cast<double> (nbIter);

    return bestClipping;
}


std::map<std::string, double> adjustRanges(Clipping clippingMode, std::map<std::string, double> valueRanges, std::uint8_t nbBits, std::shared_ptr<GraphView> graphView, std::vector<std::shared_ptr<Tensor>> inputDataSet, bool useCuda, bool verbose)
{
    double clipping = 1.0f;

    int nbBins = (1 << (nbBits + 4)) ; // XXX Enhance this !!!

    if (clippingMode != Clipping::MAX)
    {
        if (verbose)
            Log::info(" === CLIPPING VALUES === ");

        std::map<std::string, std::vector<int>> histograms = computeHistograms(valueRanges, nbBins, graphView, inputDataSet, useCuda);

        for (std::shared_ptr<Node> node : graphView->getNodes())
        {
            if (node->type() == "Scaling")
            {
                std::vector<int> histogram = histograms[node->name()];

                if (clippingMode == Clipping::MSE) 
                    clipping = computeMEClipping(histogram, nbBits, 2.0);
                if (clippingMode == Clipping::AA) 
                    clipping = computeMEClipping(histogram, nbBits, 1.0);
                if (clippingMode == Clipping::KL) 
                    clipping = computeKLClipping(histogram, nbBits);

                if (verbose)
                    Log::info(" {:.6f} ({})", clipping, node->name());
                
                valueRanges[node->name()] *= clipping;
            }
        }
    }
    

    return valueRanges;
}

}
