/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include "aidge/operator/FixedQ.hpp"

#include <memory>
#include <string>

#include "aidge/data/Tensor.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"

const std::string Aidge::FixedQ_Op::Type = "FixedQ";

Aidge::FixedQ_Op::FixedQ_Op(const Aidge::FixedQ_Op& op)
    : OperatorTensor(op),
      mAttributes(op.mAttributes)
{
    if (op.mImpl){
        SET_IMPL_MACRO(FixedQ_Op, *this, op.backend());
    }else{
        mImpl = nullptr;
    }
}

std::set<std::string> Aidge::FixedQ_Op::getAvailableBackends() const {
    return Registrar<FixedQ_Op>::getKeys();
}

void Aidge::FixedQ_Op::setBackend(const std::string& name, DeviceIdx_t device) {
    SET_IMPL_MACRO(FixedQ_Op, *this, name);
    mOutputs[0]->setBackend(name, device);
}

////////////////////////////////////////////////////////////////////////////////

std::shared_ptr<Aidge::Node> Aidge::FixedQ(std::size_t nbBits,
                            float span,
                            bool isOutputUnsigned,
                            const std::string& name) {
    return std::make_shared<Node>(std::make_shared<FixedQ_Op>(nbBits, span, isOutputUnsigned), name);
}