/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include "aidge/operator/LSQ.hpp"

#include <memory>
#include <string>

#include "aidge/data/Tensor.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"

const std::string Aidge::LSQ_Op::Type = "LSQ";

bool Aidge::LSQ_Op::forwardDims(bool /*allowDataDependency*/) {

    // TODO : check if the step size is a scalar !
    if (inputsAssociated()) {
        const auto inputsDims = getInput(0)->dims();
        mOutputs[0]->resize(inputsDims);
        return true;
    }
    return false;
}

std::set<std::string> Aidge::LSQ_Op::getAvailableBackends() const {
    return Registrar<LSQ_Op>::getKeys();
}

void Aidge::LSQ_Op::setBackend(const std::string& name, DeviceIdx_t device) {
    SET_IMPL_MACRO(LSQ_Op, *this, name);
    mOutputs[0]->setBackend(name, device);

    // By default, automatically set backend for alphas inputs
    if (getInput(1)) {
        getInput(1)->setBackend(name, device);
    }
    else {
        Log::notice("LSQ_Op::setBackend(): could not set backend for step_size input, because input is not connected");
    }
}
