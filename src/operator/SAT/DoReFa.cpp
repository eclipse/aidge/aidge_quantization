/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include "aidge/operator/SAT/DoReFa.hpp"

#include <memory>
#include <string>

#include "aidge/data/Tensor.hpp"
#include "aidge/utils/Types.h"

namespace Aidge {

const std::string DoReFa_Op::Type = "DoReFa";

DoReFa_Op::DoReFa_Op(const DoReFa_Op& op)
    : OperatorTensor(op),
      mAttributes(op.mAttributes)
{
    if (op.mImpl) {
        SET_IMPL_MACRO(DoReFa_Op, *this, op.backend());
    } else {
        mImpl = nullptr;
    }
}

std::shared_ptr<Operator> DoReFa_Op::clone() const {
    return std::make_shared<DoReFa_Op>(*this);
}

std::set<std::string> DoReFa_Op::getAvailableBackends() const {
    return Registrar<DoReFa_Op>::getKeys();
}

void DoReFa_Op::setBackend(const std::string& name, DeviceIdx_t device) {
    SET_IMPL_MACRO(DoReFa_Op, *this, name);
    mOutputs[0]->setBackend(name, device);
}

////////////////////////////////////////////////////////////////////////////////

std::shared_ptr<Node> DoReFa(size_t range, DoReFaMode mode, const std::string& name) {
    return std::make_shared<Node>(std::make_shared<DoReFa_Op>(range, mode), name);
}

}  // namespace Aidge