/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include "aidge/operator/SAT/TanhClamp.hpp"

#include <memory>
#include <string>

#include "aidge/data/Tensor.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"

const std::string Aidge::TanhClamp_Op::Type = "TanhClamp";

Aidge::TanhClamp_Op::TanhClamp_Op(const Aidge::TanhClamp_Op& op)
    : OperatorTensor(op)
{
    if (op.mImpl) {
        SET_IMPL_MACRO(TanhClamp_Op, *this, op.backend());
    } else {
        mImpl = nullptr;
    }
}

std::shared_ptr<Aidge::Operator> Aidge::TanhClamp_Op::clone() const {
    return std::make_shared<TanhClamp_Op>(*this);
}

bool Aidge::TanhClamp_Op::forwardDims(bool /*allowDataDependency*/) {

    if (inputsAssociated()) {
        const auto inputsDims = getInput(0)->dims();
        mOutputs[0]->resize(inputsDims);
        mOutputs[1]->resize({1});
        return true;
    }
    return false;
}

std::set<std::string> Aidge::TanhClamp_Op::getAvailableBackends() const {
    return Registrar<TanhClamp_Op>::getKeys();
}

void Aidge::TanhClamp_Op::setBackend(const std::string& name, DeviceIdx_t device) {
    SET_IMPL_MACRO(TanhClamp_Op, *this, name);
    mOutputs[0]->setBackend(name, device);

    // Scale output is always on CPU for now
    mOutputs[1]->setBackend("cpu"); // XXX why ?
}

////////////////////////////////////////////////////////////////////////////////

std::shared_ptr<Aidge::Node> Aidge::TanhClamp(const std::string& name) {
    return std::make_shared<Node>(std::make_shared<TanhClamp_Op>(), name);
}