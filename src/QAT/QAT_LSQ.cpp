/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include "aidge/quantization/QAT/QAT_LSQ.hpp"
#include "aidge/operator/LSQ.hpp"
#include "aidge/operator/ReLU.hpp"


#include "aidge/data/Tensor.hpp"
#include "aidge/graph/GraphView.hpp"
#include "aidge/scheduler/SequentialScheduler.hpp"
#include "aidge/scheduler/Scheduler.hpp"
#include "aidge/graph/Matching.hpp"
#include "aidge/recipes/QuantRecipes.hpp"

namespace Aidge {

void QuantLSQ::insertQuantizers(std::shared_ptr<GraphView> graphView, size_t nbBits, float stepSize)
{
    const auto matches = SinglePassGraphMatching(graphView).match("(Conv2D#|FC#)");

    for (const auto& match : matches) 
    {
        auto linearNode = match.graph->rootNode(); 

        std::pair<int, int> signedRange = {-std::pow(2, nbBits - 1), std::pow(2, nbBits - 1) - 1};
        std::pair<int, int> unsignedRange = {0, std::pow(2, nbBits) - 1};

        // INPUT QUANTIZERS INSERTION

        // TODO : double check this, and use createUniqueName()
        auto inputQuantizerName = makeUniqueName(linearNode->name() + "_lsq_i", graphView);  
        auto inputQuantizerNode = LSQ(signedRange, inputQuantizerName);

        // Set the step size

        auto inputStepSizeOp = inputQuantizerNode->getParent(1)->getOperator();
        auto inputStepSizeTensor = std::make_shared<Tensor>(Array1D<float, 1>({{stepSize}}));
        inputStepSizeOp->setOutput(0, inputStepSizeTensor);

        // Absorb the ReLU when possible ...

        // XXX is this safe ???
        bool nodeHasParent = static_cast<bool> (linearNode->getParents()[0]); 
        // bool nodeHasParent = (linearNode->getParents().size() != 0);

        if (nodeHasParent) {
            auto parentNode = linearNode->getParents()[0];
            if (parentNode->type() == "ReLU") {
                auto inputQuantizerOp = std::static_pointer_cast<LSQ_Op> (inputQuantizerNode->getOperator());
                inputQuantizerOp->range() = unsignedRange;
                graphView->replace({parentNode}, {}); 
            }
        }

        // We need to handle the case where the linear node is the first one ...

        if (nodeHasParent) {
            graphView->insertParent(linearNode, inputQuantizerNode, 0, 0, 0);
        } else {
            inputQuantizerNode->addChild(graphView);
            graphView->add(inputQuantizerNode);
        }

        // PARAM QUANTIZERS INSERTION

        // TODO : double check this, and use createUniqueName()
        auto paramQuantizerName = makeUniqueName(linearNode->name() + "_lsq_p", graphView);  
        auto paramQuantizerNode = LSQ(signedRange, paramQuantizerName); 
        graphView->insertParent(linearNode, paramQuantizerNode, 1, 0, 0);

        // Set the step size

        auto paramStepSizeOp = paramQuantizerNode->getParent(1)->getOperator();
        auto paramStepSizeTensor = std::make_shared<Tensor>(Array1D<float, 1>({{stepSize}}));
        paramStepSizeOp->setOutput(0, paramStepSizeTensor);
    }

}

static float getTensorAbsMean(std::shared_ptr<Tensor> tensor)
{
    auto backend = tensor->backend();
    if (backend == "cuda")
        tensor->setBackend("cpu");

    float acc = 0;
    float* castedTensor = static_cast<float *> (tensor->getImpl()->rawPtr());
    for(std::size_t i = 0; i < tensor->size(); i++)
        acc += std::abs(castedTensor[i]);
    acc /= static_cast<float> (tensor->size());

    if (backend == "cuda")
        tensor->setBackend("cuda");

    return acc;
}

static std::map<std::string, float> collectInputStats(std::shared_ptr<GraphView> graphView, std::shared_ptr<Tensor> calibrationData, bool useCuda)
{
    // Propagate the calibration tensor

    SequentialScheduler scheduler(graphView);
    scheduler.resetScheduling();
    scheduler.forward(true, {calibrationData});

    // Store the input tensor statistics

    if (useCuda)
        graphView->setBackend("cpu"); 

    std::map<std::string, float> inputStats;
    for (auto node : graphView->getNodes())
    {
        if (node->type() == "FC" || node->type() == "Conv2D") // TODO: use graph matching !!!
        {
            const auto op = std::static_pointer_cast<LSQ_Op>(node->getOperator());
            float inputAbsMean = getTensorAbsMean(op->getInput(0));
            inputStats.insert(std::make_pair(node->name(), inputAbsMean));
            fmt::println("{} -> {}", node->name(), inputAbsMean);
        }
    }

    if (useCuda)
        graphView->setBackend("cuda");

    return inputStats;
}

static std::map<std::string, float> collectParamStats(std::shared_ptr<GraphView> graphView, bool useCuda)
{
    if (useCuda)
        graphView->setBackend("cpu");

    std::map<std::string, float> paramStats;
    for (auto node : graphView->getNodes())
    {
        if (node->type() == "FC" || node->type() == "Conv2D") // TODO: use graph matching !!!
        {
            const auto op = std::static_pointer_cast<LSQ_Op>(node->getOperator());
            float paramAbsMean = getTensorAbsMean(op->getInput(1));
            paramStats.insert(std::make_pair(node->name(), paramAbsMean));
            fmt::println("{} -> {}", node->name(), paramAbsMean);
        }
    }
    
    if (useCuda)
        graphView->setBackend("cuda");

    return paramStats;
}

static void adjustQuantizersStepSizes(std::shared_ptr<GraphView> graphView, std::map<std::string, float> inputStats, std::map<std::string, float> paramStats)
{
    const auto matches = SinglePassGraphMatching(graphView).match("(Conv2D#|FC#)");

    for (const auto& match : matches) 
    {
        auto linearNode = match.graph->rootNode();

        // INPUT QUANTIZERS STEP-SIZES

        auto inputQuantNode = linearNode->getParent(0);
        auto inputQuantOp = std::static_pointer_cast<LSQ_Op>(inputQuantNode->getOperator());

        float absMean = inputStats[linearNode->name()];
        float stepSize = 2.0f * (absMean / std::sqrt(inputQuantOp->range().second));

        auto inputStepSizeOp = inputQuantNode->getParent(1)->getOperator();
        // XXX inputStepSizeOp->setOutput(0, std::make_shared<Tensor>(Array1D<float, 1>({{stepSize}})));
        auto inputStepSizeTensor = std::make_shared<Tensor>(Array1D<float, 1>({{stepSize}}));
        inputStepSizeOp->setOutput(0, inputStepSizeTensor);

        // PARAM QUANTIZERS STEP-SIZES

        auto paramQuantNode = linearNode->getParent(1);
        auto paramQuantOp = std::static_pointer_cast<LSQ_Op>(paramQuantNode->getOperator());

        absMean = paramStats[linearNode->name()];
        stepSize = 2.0f * (absMean / std::sqrt(paramQuantOp->range().second));

        auto paramStepSizeOp = paramQuantNode->getParent(1)->getOperator();
        // XXX paramStepSizeOp->setOutput(0, std::make_shared<Tensor>(Array1D<float, 1>({{stepSize}})));
        auto paramStepSizeTensor = std::make_shared<Tensor>(Array1D<float, 1>({{stepSize}}));
        paramStepSizeOp->setOutput(0, paramStepSizeTensor);
    }
}

void QuantLSQ::insertAndInitQuantizers(std::shared_ptr<GraphView> graphView, size_t nbBits, std::shared_ptr<Tensor> calibrationData)
{
    bool useCuda = (calibrationData->backend() == "cuda");

    // Collect the tensor statisics
    auto inputStats = collectInputStats(graphView, calibrationData, useCuda);

    auto paramStats = collectParamStats(graphView, useCuda);

    // Insert the quantizers
    insertQuantizers(graphView, nbBits, 1.0);

    // Adjust the quantizers step-sizes
    adjustQuantizersStepSizes(graphView, inputStats, paramStats);
}

}