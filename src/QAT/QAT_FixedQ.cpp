/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include "aidge/quantization/QAT/QAT_FixedQ.hpp"
#include "aidge/operator/FixedQ.hpp"

#include "aidge/data/Tensor.hpp"
#include "aidge/graph/GraphView.hpp"
#include "aidge/scheduler/SequentialScheduler.hpp"
#include "aidge/scheduler/Scheduler.hpp"
#include "aidge/graph/Matching.hpp"

namespace Aidge {

void QuantFixedQ::insertQuantizers(std::shared_ptr<GraphView> graphView, size_t nbBits, float span)
{
    const auto matches = SinglePassGraphMatching(graphView).match("(Conv2D#|FC#)");

    for (const auto& match : matches) 
    {
        auto linearNode = match.graph->rootNode(); 

        // INPUT QUANTIZERS INSERTION

        auto inputQuantizerName = linearNode->name() + "_fixedq_i";  // TODO : double check this, and use createUniqueName()
        auto inputQuantizerNode = FixedQ(nbBits, span, false, inputQuantizerName);

        // Absorb the ReLU when possible ...

        bool nodeHasParent = static_cast<bool> (linearNode->getParents()[0]); // XXX is this safe ???

        if (nodeHasParent) {
            auto parentNode = linearNode->getParents()[0];
            if (parentNode->type() == "ReLU") {
                auto inputQuantizerOp = std::static_pointer_cast<FixedQ_Op> (inputQuantizerNode->getOperator());
                inputQuantizerOp->isOutputUnsigned() = true;
                graphView->replace({parentNode}, {}); 
            }
        }

        // We need to handle the case where the linear node is the first one ...

        if (nodeHasParent) {
            graphView->insertParent(linearNode, inputQuantizerNode, 0, 0, 0);
        } else {
            inputQuantizerNode->addChild(graphView);
            graphView->add(inputQuantizerNode);
        }

        // PARAM QUANTIZERS INSERTION

        auto paramQuantizerName = linearNode->name() + "_fixedq_p";  // TODO : double check this, and use createUniqueName()
        auto paramQuantizerNode = FixedQ(nbBits, span, false, paramQuantizerName); 
        graphView->insertParent(linearNode, paramQuantizerNode, 1, 0, 0);
    }
}

static float getTensorStd(std::shared_ptr<Tensor> tensor)
{
    float acc = 0;
    float * castedTensor = static_cast<float *> (tensor->getImpl()->rawPtr());
    for(std::size_t i = 0; i < tensor->size(); i++)
        acc += castedTensor[i] * castedTensor[i];
    acc /= static_cast<float> (tensor->size());
    return std::sqrt(acc);
}

static std::map<std::string, float> collectInputStats(std::shared_ptr<GraphView> graphView, std::shared_ptr<Tensor> calibrationData)
{
    // Propagate the calibration tensor

    SequentialScheduler scheduler(graphView);
    scheduler.resetScheduling();
    scheduler.forward(true, {calibrationData});

    // Store the input tensor statistics

    std::map<std::string, float> inputStats;
    for (auto node : graphView->getNodes())
    {
        if (node->type() == "FC" || node->type() == "Conv2D") // TODO: use graph matching !!!
        {
            const auto op = std::static_pointer_cast<FixedQ_Op>(node->getOperator());
            float inputStd = getTensorStd(op->getInput(0));
            inputStats.insert(std::make_pair(node->name(), inputStd));
            fmt::println("{} -> {}", node->name(), inputStd);
        }
    }

    return inputStats;
}

static std::map<std::string, float> collectParamStats(std::shared_ptr<GraphView> graphView)
{
    std::map<std::string, float> paramStats;
    for (auto node : graphView->getNodes())
    {
        if (node->type() == "FC" || node->type() == "Conv2D") // TODO: use graph matching !!!
        {
            const auto op = std::static_pointer_cast<FixedQ_Op>(node->getOperator());
            float paramStd = getTensorStd(op->getInput(1));
            paramStats.insert(std::make_pair(node->name(), paramStd));
            fmt::println("{} -> {}", node->name(), paramStd);
        }
    }
    
    return paramStats;
}

static void adjustQuantizersSpans(std::shared_ptr<GraphView> graphView, std::map<std::string, float> inputStats, std::map<std::string, float> paramStats, float scale = 4.0f)
{
    const auto matches = SinglePassGraphMatching(graphView).match("(Conv2D#|FC#)");

    for (const auto& match : matches) 
    {
        auto linearNode = match.graph->rootNode();

        // Adjust the input quantizers spans

        auto inputQuantNode = linearNode->getParent(0);
        auto inputQuantOp = std::static_pointer_cast<FixedQ_Op>(inputQuantNode->getOperator());
        inputQuantOp->span() = inputStats[linearNode->name()] * scale;

        // Adjust the param quantizers spans

        auto paramQuantNode = linearNode->getParent(1);
        auto paramQuantOp = std::static_pointer_cast<FixedQ_Op>(paramQuantNode->getOperator());
        paramQuantOp->span() = paramStats[linearNode->name()] * scale;
    }
}

void QuantFixedQ::insertAndInitQuantizers(std::shared_ptr<GraphView> graphView, size_t nbBits, std::shared_ptr<Tensor> calibrationData, float scale)
{
    // Collect the tensor statisics
    auto inputStats = collectInputStats(graphView, calibrationData);
    auto paramStats = collectParamStats(graphView);

    // Insert the quantizers
    insertQuantizers(graphView, nbBits, 1.0);

    // Adjust the quantizers spans
    adjustQuantizersSpans(graphView, inputStats, paramStats, scale);
}

void QuantFixedQ::devQAT(std::shared_ptr<GraphView> graphView) 
{
    SequentialScheduler scheduler(graphView);
    scheduler.generateScheduling();
    auto s = scheduler.getStaticScheduling();
    for (std::shared_ptr<Node> node : s)
        fmt::println(" name : {}", node->name());
}

}