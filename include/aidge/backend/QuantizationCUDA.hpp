// /********************************************************************************
//  * Copyright (c) 2023 CEA-List
//  *
//  * This program and the accompanying materials are made available under the
//  * terms of the Eclipse Public License 2.0 which is available at
//  * http://www.eclipse.org/legal/epl-2.0.
//  *
//  * SPDX-License-Identifier: EPL-2.0
//  *
//  ********************************************************************************/
#ifndef AIDGE_QUANTIZATION_CUDA_IMPORTS_H_
#define AIDGE_QUANTIZATION_CUDA_IMPORTS_H_

#include "aidge/backend/cuda/operator/LSQImpl.hpp"
#include "aidge/backend/cuda/operator/FixedQImpl.hpp"


// ...

#endif /* AIDGE_QUANTIZATION_CUDA_IMPORTS_H_ */