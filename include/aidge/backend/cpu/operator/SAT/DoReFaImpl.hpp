/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CPU_OPERATOR_DOREFAIMPL_H_
#define AIDGE_CPU_OPERATOR_DOREFAIMPL_H_

#include <cstddef>  // std::size_t
#include <memory>
#include <tuple>    // std::tuple
#include <vector>

#include "aidge/backend/cpu/operator/OperatorImpl.hpp"
#include "aidge/operator/SAT/DoReFa.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"

namespace Aidge {

// Operator implementation entry point for the backend
using DoReFaImpl_cpu = OperatorImpl_cpu<DoReFa_Op,
    void(const std::size_t, float, DoReFaMode, const void*, void*),
    void(const std::size_t, float, DoReFaMode, const void*, const void*, void*)>;

// Implementation entry point registration to Operator
REGISTRAR(DoReFa_Op, "cpu", Aidge::DoReFaImpl_cpu::create);

}  // namespace Aidge

#endif /* AIDGE_CPU_OPERATOR_DOREFAIMPL_H_ */
