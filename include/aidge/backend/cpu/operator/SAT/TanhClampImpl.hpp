/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CPU_OPERATOR_TANHCLAMPIMPL_H_
#define AIDGE_CPU_OPERATOR_TANHCLAMPIMPL_H_

#include <cstddef>  // std::size_t
#include <memory>
#include <tuple>    // std::tuple
#include <vector>

#include "aidge/backend/cpu/operator/OperatorImpl.hpp"
#include "aidge/operator/SAT/TanhClamp.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"

namespace Aidge {
// Operator implementation entry point for the backend
using TanhClampImpl_cpu = OperatorImpl_cpu<TanhClamp_Op,
    void(const std::size_t, const void*, void*, void*),
    void(const std::size_t, const void*, const void*, void*, void*)>;

// Implementation entry point registration to Operator
REGISTRAR(TanhClamp_Op, "cpu", Aidge::TanhClampImpl_cpu::create);
}  // namespace Aidge

#endif /* AIDGE_CPU_OPERATOR_TANHCLAMPIMPL_H_ */
