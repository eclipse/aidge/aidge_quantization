/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CPU_OPERATOR_TANHCLAMPIMPL_KERNELS_H_
#define AIDGE_CPU_OPERATOR_TANHCLAMPIMPL_KERNELS_H_

#include <cstddef>  // std::size_t
#include <memory>
#include <tuple>    // std::tuple
#include <vector>

#include "aidge/backend/cpu/operator/OperatorImpl.hpp"
#include "aidge/backend/cpu/operator/SAT/TanhClampImpl.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"

namespace Aidge {
// Kernels
template <class I, class O>
void TanhClampImpl_cpu_forward_kernel(std::size_t inputLength,
                                      const void* input_,
                                      void* scaling_,
                                      void* output_)
{
    const I* input = static_cast<const I*>(input_);
    I scaling = *static_cast<I*>(scaling_);
    O* output = static_cast<O*>(output_);

    const auto minMax = std::minmax_element(input, input + inputLength);
    const auto absMax = std::max(std::abs(*(minMax.first)), std::abs(*(minMax.second)));
    scaling = std::tanh(absMax);

    //#pragma omp parallel for if (inputLength > 1024)
    for (unsigned int i = 0; i < inputLength; ++i) {
        output[i] = std::tanh(input[i]) / scaling;
    }

    // Set the scaling output ...
    *(static_cast<I*> (scaling_)) = scaling;
}

template <class I, class GI, class GO>
void TanhClampImpl_cpu_backward_kernel(const std::size_t inputLength,
                                 const void* input_,
                                 const void* scaling_,
                                 const void* grad_output_,
                                 void* grad_input_)
{
    const I* input = static_cast<const I*>(input_);
    const I scaling = *static_cast<const I*>(scaling_);
    const GO* grad_output = static_cast<const GO*>(grad_output_);
    GI* grad_input = static_cast<GI*>(grad_input_);

    //#pragma omp parallel for if (inputLength > 1024)
    for (unsigned int i = 0; i < inputLength; ++i) {
        const auto inv_cosh = GO(1 / std::cosh(input[i]));
        const auto grad = inv_cosh * inv_cosh * GO(1 / scaling);
        grad_input[i] = grad_output[i] * grad;
    }
}


// Kernels registration to implementation entry point
REGISTRAR(TanhClampImpl_cpu,
    {{DataType::Int32, DataFormat::NCHW}, {DataType::Int32, DataFormat::NCHW}},
    {ProdConso::inPlaceModel, Aidge::TanhClampImpl_cpu_forward_kernel<int, int>, Aidge::TanhClampImpl_cpu_backward_kernel<int, int, int>});
REGISTRAR(TanhClampImpl_cpu,
    {{DataType::Float32, DataFormat::NCHW}, {DataType::Float32, DataFormat::NCHW}},
    {ProdConso::inPlaceModel, Aidge::TanhClampImpl_cpu_forward_kernel<float, float>, Aidge::TanhClampImpl_cpu_backward_kernel<float, float, float>});
REGISTRAR(TanhClampImpl_cpu,
    {{DataType::Float64, DataFormat::NCHW}, {DataType::Float64, DataFormat::NCHW}},
    {ProdConso::inPlaceModel, Aidge::TanhClampImpl_cpu_forward_kernel<double, double>, Aidge::TanhClampImpl_cpu_backward_kernel<double, double, double>});

}  // namespace Aidge

#endif /* AIDGE_CPU_OPERATOR_TANHCLAMPIMPL_KERNELS_H_ */
