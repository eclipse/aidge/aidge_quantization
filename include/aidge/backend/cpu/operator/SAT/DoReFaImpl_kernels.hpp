/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CPU_OPERATOR_DOREFAIMPL_FORWARD_KERNEL_H_
#define AIDGE_CPU_OPERATOR_DOREFAIMPL_FORWARD_KERNEL_H_

#include "aidge/utils/Registrar.hpp"

#include "aidge/backend/cpu/operator/SAT/DoReFaImpl.hpp"

namespace Aidge {

template <class I, class O>
void DoReFaImpl_cpu_forward_kernel(std::size_t inputLength,
                                      float range,
                                      DoReFaMode mode,
                                      const void* input_,
                                      void* output_)
{
    const I* input = static_cast<const I*>(input_);
    O* output = static_cast<O*>(output_);

    // Dorefa Quantization
    //#pragma omp parallel for if (inputLength > 1024)
    for (unsigned int i = 0; i < inputLength; ++i) {
        if (mode == DoReFaMode::Default) {
            auto q = I(0.5) * (input[i] + I(1.0));
            q = O(1.0f / range) * O(std::rintf(q * range));
            output[i] = q * O(2.0) - O(1.0);
        }
        else {
            output[i] = O(1.0f / range) * O(std::rintf(input[i] * range));
        }
    }
}

template <class I, class GI, class GO>
void DoReFaImpl_cpu_backward_kernel(const std::size_t inputLength,
                                 float /*range*/,
                                 DoReFaMode /*mode*/,
                                 const void* /*input_*/,
                                 const void* grad_output_,
                                 void* grad_input_)
{
    const GO* grad_output = static_cast<const GO*>(grad_output_);
    GI* grad_input = static_cast<GI*>(grad_input_);

    //#pragma omp parallel for if (inputLength > 1024)
    for (unsigned int i = 0; i < inputLength; ++i) {
        grad_input[i] = grad_output[i];
    }
}

// Kernels registration to implementation entry point
REGISTRAR(DoReFaImpl_cpu,
    {{DataType::Int32, DataFormat::NCHW}, {DataType::Int32, DataFormat::NCHW}},
    {ProdConso::inPlaceModel, Aidge::DoReFaImpl_cpu_forward_kernel<int, int>, Aidge::DoReFaImpl_cpu_backward_kernel<int, int, int>});
REGISTRAR(DoReFaImpl_cpu,
    {{DataType::Float32, DataFormat::NCHW}, {DataType::Float32, DataFormat::NCHW}},
    {ProdConso::inPlaceModel, Aidge::DoReFaImpl_cpu_forward_kernel<float, float>, Aidge::DoReFaImpl_cpu_backward_kernel<float, float, float>});
REGISTRAR(DoReFaImpl_cpu,
    {{DataType::Float64, DataFormat::NCHW}, {DataType::Float64, DataFormat::NCHW}},
    {ProdConso::inPlaceModel, Aidge::DoReFaImpl_cpu_forward_kernel<double, double>, Aidge::DoReFaImpl_cpu_backward_kernel<double, double, double>});

}  // namespace Aidge

#endif /* AIDGE_CPU_OPERATOR_DOREFAIMPL_FORWARD_KERNEL_H_ */
