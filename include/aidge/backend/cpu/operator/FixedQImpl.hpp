/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CPU_OPERATOR_FIXEDQIMPL_H_
#define AIDGE_CPU_OPERATOR_FIXEDQIMPL_H_

#include <cstddef>  // std::size_t
#include <memory>
#include <tuple>    // std::tuple
#include <vector>

#include "aidge/backend/cpu/operator/OperatorImpl.hpp"
#include "aidge/operator/FixedQ.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"

namespace Aidge {

// Operator implementation entry point for the backend
using FixedQImpl_cpu = OperatorImpl_cpu<FixedQ_Op,
    void(const std::size_t, const float, const bool, const std::size_t, const void*, void*),
    void(const std::size_t, const void*, void*)>;

// Implementation entry point registration to Operator
REGISTRAR(FixedQ_Op, "cpu", Aidge::FixedQImpl_cpu::create);

}  // namespace Aidge

#endif /* AIDGE_CPU_OPERATOR_FIXEDQIMPL_H_ */
