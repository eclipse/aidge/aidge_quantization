/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CPU_OPERATOR_LSQIMPL_FORWARD_KERNEL_H_
#define AIDGE_CPU_OPERATOR_LSQIMPL_FORWARD_KERNEL_H_

#include "aidge/utils/Registrar.hpp"

#include "aidge/backend/cpu/operator/LSQImpl.hpp"

namespace Aidge {
template <class I, class O>
void LSQImpl_cpu_forward_kernel(std::size_t inputLength,
                                const std::pair<int, int>& range,
                                const void* input_,
                                const void* stepSize_,
                                void* output_)
{
    const I* input = static_cast<const I*>(input_);
    const I* stepSize = static_cast<const I*>(stepSize_);
    O* output = static_cast<O*>(output_);

    const O bitRangesLowerBound = static_cast<O>(range.first * stepSize[0]);
    const O bitRangesUpperBound = static_cast<O>(range.second * stepSize[0]);

//#pragma omp parallel for if (inputLength > 16)
    for (unsigned int i = 0; i < inputLength; i++) {
        const O qData = input[i] / stepSize[0];

        output[i] =
            (qData <= static_cast<O>(range.first)) ? bitRangesLowerBound :
            (qData >= static_cast<O>(range.second)) ? bitRangesUpperBound :
                                          std::round(qData) * stepSize[0];
    }
}

template <class I, class GI, class GO>
void LSQImpl_cpu_backward_kernel(const std::size_t inputLength,
                                 const std::pair<int, int>& range,
                                 const void* input_,
                                 const void* stepSize_,
                                 const void* grad_output_,
                                 void* grad_input_,
                                 void* grad_stepSize_)
{
    const I* input = static_cast<const I*>(input_);
    const I* stepSize = static_cast<const I*>(stepSize_);
    const GO* grad_output = static_cast<const GO*>(grad_output_);
    GI* grad_input = static_cast<GI*>(grad_input_);
    GI* grad_stepSize = static_cast<GI*>(grad_stepSize_);

    GI diffStepSize = GI(0.0);

#pragma omp parallel for schedule(static, 256) reduction(+:diffStepSize) if(inputLength > 16)
    for(unsigned int i=0; i < inputLength / 4; i++) {
        const GI fullPrecScale_1 = input[4*i] / stepSize[0];
        const GI fullPrecScale_2 = input[4*i+1] / stepSize[0];
        const GI fullPrecScale_3 = input[4*i+2] / stepSize[0];
        const GI fullPrecScale_4 = input[4*i+3] / stepSize[0];
        /*****************Features Gradient Computation********************/
        // STE method is simply applied
        grad_input[4*i] += grad_output[4*i]*((fullPrecScale_1 <= static_cast<GI>(range.first)) ? GI(0.0) :
                                                          (fullPrecScale_1 >= static_cast<GI>(range.second)) ? GI(0.0) :
                                                          GI(1.0));
        grad_input[4*i+1] += grad_output[4*i+1]*((fullPrecScale_2 <= static_cast<GI>(range.first)) ? GI(0.0) :
                                                              (fullPrecScale_2 >= static_cast<GI>(range.second)) ? GI(0.0) :
                                                              GI(1.0));
        grad_input[4*i+2] += grad_output[4*i+2]*((fullPrecScale_3 <= static_cast<GI>(range.first)) ? GI(0.0) :
                                                              (fullPrecScale_3 >= static_cast<GI>(range.second)) ? GI(0.0) :
                                                              GI(1.0));
        grad_input[4*i+3] += grad_output[4*i+3]*((fullPrecScale_4 <= static_cast<GI>(range.first)) ? GI(0.0) :
                                                              (fullPrecScale_4 >= static_cast<GI>(range.second)) ? GI(0.0) :
                                                              GI(1.0));

        /*****************Step Size Gradient Computation******************/
        //1st: clip the gradient in interval [rangeMin, rangeMax] and take account of qError
        GI qData_1 = fullPrecScale_1;
        qData_1 = ((qData_1 <= static_cast<GI>(range.first)) ? static_cast<GI>(range.first) :
                   (qData_1 >= static_cast<GI>(range.second)) ? static_cast<GI>(range.second) :
                   round(qData_1) - qData_1);
        GI qData_2 = fullPrecScale_2;
        qData_2 = ((qData_2 <= static_cast<GI>(range.first)) ? static_cast<GI>(range.first) :
                  (qData_2 >= static_cast<GI>(range.second)) ? static_cast<GI>(range.second) :
                  round(qData_2) - qData_2);
        GI qData_3 = fullPrecScale_3;
        qData_3 = ((qData_3 <= static_cast<GI>(range.first)) ? static_cast<GI>(range.first) :
                  (qData_3 >= static_cast<GI>(range.second)) ? static_cast<GI>(range.second) :
                  round(qData_3) - qData_3);
        GI qData_4 = fullPrecScale_4;
        qData_4 = ((qData_4 <= static_cast<GI>(range.first)) ? static_cast<GI>(range.first) :
                  (qData_4 >= static_cast<GI>(range.second)) ? static_cast<GI>(range.second) :
                  round(qData_4) - qData_4);
        //2nd: Multiplie backward data with clipped grad
        diffStepSize += ((qData_1*grad_output[4*i] + qData_2*grad_output[4*i+1])+(qData_3*grad_output[4*i+2] + qData_4*grad_output[4*i+3]));
    }

    // Process remaining
    for(unsigned int i=inputLength-inputLength%4; i<inputLength; ++i) {
        const GI fullPrecScale = input[i] / stepSize[0];
        grad_input[i] += grad_output[i]*((fullPrecScale <= static_cast<GI>(range.first)) ? GI(0.0) :
                                        (fullPrecScale >= static_cast<GI>(range.second)) ? GI(0.0) :
                                        GI(1.0));
        GI qData = fullPrecScale;
        qData = ((qData <= static_cast<GI>(range.first)) ? static_cast<GI>(range.first) :
                (qData >= static_cast<GI>(range.second)) ? static_cast<GI>(range.second) :
                round(qData) - qData);
        diffStepSize += qData*grad_output[i];
    }

    const GI gradScaleFactor = static_cast<GI>(1.0f / std::sqrt(inputLength * range.second));
    // 3rd: Multiply Step Size gradient with scale factor
    grad_stepSize[0] += diffStepSize * gradScaleFactor;
}


// Kernels registration to implementation entry point
REGISTRAR(LSQImpl_cpu,
    {{DataType::Float16, DataFormat::NCHW}, {DataType::Float16, DataFormat::NCHW}},
    {ProdConso::inPlaceModel, Aidge::LSQImpl_cpu_forward_kernel<half_float::half, half_float::half>, Aidge::LSQImpl_cpu_backward_kernel<half_float::half, half_float::half, half_float::half>});
REGISTRAR(LSQImpl_cpu,
    {{DataType::Float32, DataFormat::NCHW}, {DataType::Float32, DataFormat::NCHW}},
    {ProdConso::inPlaceModel, Aidge::LSQImpl_cpu_forward_kernel<float, float>, Aidge::LSQImpl_cpu_backward_kernel<float, float, float>});
REGISTRAR(LSQImpl_cpu,
    {{DataType::Float64, DataFormat::NCHW}, {DataType::Float64, DataFormat::NCHW}},
    {ProdConso::inPlaceModel, Aidge::LSQImpl_cpu_forward_kernel<double, double>, Aidge::LSQImpl_cpu_backward_kernel<double, double, double>});

}  // namespace Aidge

#endif /* AIDGE_CPU_OPERATOR_LSQIMPL_FORWARD_KERNEL_H_ */
