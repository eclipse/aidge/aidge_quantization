/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CPU_OPERATOR_LSQIMPL_H_
#define AIDGE_CPU_OPERATOR_LSQIMPL_H_

#include <cstddef>  // std::size_t
#include <memory>
#include <tuple>    // std::tuple
#include <vector>

#include "aidge/backend/cpu/operator/OperatorImpl.hpp"
#include "aidge/operator/LSQ.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"

namespace Aidge {
// compute kernel registry for forward and backward

using LSQImpl_cpu = OperatorImpl_cpu<LSQ_Op,
    void(const std::size_t, std::pair<int, int>&, const void*, const void*, void*),
    void(const std::size_t, std::pair<int, int>&, const void*, const void*, const void*, void*,void*)>;

// Implementation entry point registration to Operator
REGISTRAR(LSQ_Op, "cpu", Aidge::LSQImpl_cpu::create);
}  // namespace Aidge

#endif /* AIDGE_CPU_OPERATOR_LSQIMPL_H_ */

