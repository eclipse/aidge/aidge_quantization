/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CPU_OPERATOR_FIXEDQIMPL_FORWARD_KERNEL_H_
#define AIDGE_CPU_OPERATOR_FIXEDQIMPL_FORWARD_KERNEL_H_

#include "aidge/utils/Registrar.hpp"
#include "aidge/backend/cpu/operator/FixedQImpl.hpp"

//#include <iostream>

namespace Aidge {
template <class I, class O>
void FixedQImpl_cpu_forward_kernel(
    std::size_t nbBits,
    float span_,
    bool isOutputUnsigned,
    std::size_t inputLenght,
    const void* input_,
    void* output_) 
{

    I span = static_cast<I> (span_);
    I stepSize = span / static_cast<I> (1 << (nbBits - 1));
    if (isOutputUnsigned) {
        stepSize /= 2;
    }

    const I upper = span - stepSize;
    const I lower = isOutputUnsigned ? 0 : -span;

    const I* input = static_cast<const I*>(input_);
    O* output = static_cast<O*>(output_);

    for (std::size_t i = 0; i < inputLenght; ++i) {
        I clipped = std::max(lower, std::min(input[i], upper));
        output[i] = std::round(clipped / stepSize) * stepSize;
    }

}

template <class GI, class GO>
void FixedQImpl_cpu_backward_kernel(
    const std::size_t inputLenght,
    const void* grad_output_,
	void* grad_input_) 
{
    const GO* grad_output = static_cast<const GO*>(grad_output_);
    GI* grad_input = static_cast<GI*>(grad_input_);

    for (std::size_t i = 0; i < inputLenght; ++i) {
        // Straight Through Estimator
        grad_input[i] = grad_output[i];
    }
}

// Kernels registration to implementation entry point
REGISTRAR(FixedQImpl_cpu,
    {{DataType::Int32, DataFormat::NCHW}, {DataType::Int32, DataFormat::NCHW}},
    {ProdConso::inPlaceModel, Aidge::FixedQImpl_cpu_forward_kernel<int, int>, Aidge::FixedQImpl_cpu_backward_kernel<int, int>});
REGISTRAR(FixedQImpl_cpu,
    {{DataType::Float32, DataFormat::NCHW}, {DataType::Float32, DataFormat::NCHW}},
    {ProdConso::inPlaceModel, Aidge::FixedQImpl_cpu_forward_kernel<float, float>, Aidge::FixedQImpl_cpu_backward_kernel<float, float>});
REGISTRAR(FixedQImpl_cpu,
    {{DataType::Float64, DataFormat::NCHW}, {DataType::Float64, DataFormat::NCHW}},
    {ProdConso::inPlaceModel, Aidge::FixedQImpl_cpu_forward_kernel<double, double>, Aidge::FixedQImpl_cpu_backward_kernel<double, double>});

}  // namespace Aidge

#endif /* AIDGE_CPU_OPERATOR_FIXEDQIMPL_FORWARD_KERNEL_H_ */
