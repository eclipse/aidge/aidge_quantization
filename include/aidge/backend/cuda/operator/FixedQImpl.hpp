/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CUDA_OPERATOR_FIXEDQIMPL_H_
#define AIDGE_CUDA_OPERATOR_FIXEDQIMPL_H_

#include <cstddef>  // std::size_t
#include <memory>
#include <tuple>    // std::tuple
#include <vector>

#include "aidge/backend/OperatorImpl.hpp"
#include "aidge/backend/cuda/operator/OperatorImpl.hpp"

#include "aidge/operator/FixedQ.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"

namespace Aidge {

// The following helper cannot be used because of extra attributes ...

using FixedQImpl_cuda = OperatorImpl_cuda<FixedQ_Op,
    void(const std::size_t, const float, const bool, const std::size_t, const void*, void*),
    void(const std::size_t, const void*, void*)>;

// Implementation entry point registration to Operator
REGISTRAR(FixedQ_Op, "cuda", Aidge::FixedQImpl_cuda::create);

}  // namespace Aidge

#endif /* AIDGE_CUDA_OPERATOR_FIXEDQIMPL_H_ */

