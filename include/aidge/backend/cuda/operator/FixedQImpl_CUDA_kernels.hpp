/********************************************************************************
 * Copyright (c) 2024 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CUDA_OPERATOR_FIXEDQIMPL_KERNELS_H_
#define AIDGE_CUDA_OPERATOR_FIXEDQIMPL_KERNELS_H_

#include "aidge/data/Data.hpp"

#include "aidge/backend/cuda/utils/CudaUtils.hpp" 
#include "aidge/backend/cuda/operator/FixedQImpl.hpp"

namespace Aidge {
template <class I, class O>
void FixedQImpl_cuda_forward_kernel(std::size_t nbBits,
                                    float span_,
                                    bool isOutputUnsigned,
                                    std::size_t inputLength,
                                    const void* input_,
                                    void* output_);

template <class GI, class GO>
void FixedQImpl_cuda_backward_kernel(const std::size_t inputLength,
                                    const void* grad_output_,
                                    void* grad_input_);


// Kernels registration to implementation entry point

//REGISTRAR(FixedQImpl_cuda,
//    {{DataType::Float16, DataFormat::NCHW}, {DataType::Float16, DataFormat::NCHW}},
//    {ProdConso::inPlaceModel, Aidge::FixedQImpl_cuda_forward_kernel<half_float::half, half_float::half>, Aidge::FixedQImpl_cuda_backward_kernel<half_float::half, half_float::half, half_float::half>});

REGISTRAR(FixedQImpl_cuda,
    {{DataType::Float32, DataFormat::NCHW}, {DataType::Float32, DataFormat::NCHW}},
    {ProdConso::inPlaceModel, Aidge::FixedQImpl_cuda_forward_kernel<float, float>, Aidge::FixedQImpl_cuda_backward_kernel<float, float>});

REGISTRAR(FixedQImpl_cuda,
    {{DataType::Float64, DataFormat::NCHW}, {DataType::Float64, DataFormat::NCHW}},
    {ProdConso::inPlaceModel, Aidge::FixedQImpl_cuda_forward_kernel<double, double>, Aidge::FixedQImpl_cuda_backward_kernel<double, double>});

}  // namespace Aidge

#endif /* AIDGE_CUDA_OPERATOR_FIXEDQIMPL_KERNELS_H_ */
