/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CUDA_OPERATOR_LSQIMPL_H_
#define AIDGE_CUDA_OPERATOR_LSQIMPL_H_

#include <cstddef>  // std::size_t
#include <memory>
#include <tuple>    // std::tuple
#include <vector>

#include "aidge/backend/OperatorImpl.hpp"
#include "aidge/operator/LSQ.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"

namespace Aidge {

// The following helper cannot be used because of extra attributes ...

// using LSQImpl_cuda = OperatorImpl_cuda<LSQ_Op,
//     void(const std::size_t, std::pair<int, int>&, const void*, const void*, void*),
//     void(const std::size_t, std::pair<int, int>&, const void*, const void*, const void*, void*, void*, void*)>;


class LSQImpl_cuda : public OperatorImpl,
    public Registrable<
        LSQImpl_cuda, 
        ImplSpec, 
        Impl<
            void(const std::size_t, std::pair<int, int>&, const void*, const void*, void*), 
            void(const std::size_t, std::pair<int, int>&, const void*, const void*, const void*, void*, void* , void*)
        >
    >
{
public:
    LSQImpl_cuda(const LSQ_Op& op) : OperatorImpl(op, "cuda") {}

    static std::unique_ptr<LSQImpl_cuda> create(const LSQ_Op& op) {
        return std::make_unique<LSQImpl_cuda>(op);
    }

    virtual std::shared_ptr<ProdConso> getProdConso() const override {
        const auto impl = Registrar<LSQImpl_cuda>::create(getBestMatch(getRequiredSpec()));
        return impl.prodConso(mOp);
    }

    virtual std::vector<ImplSpec> getAvailableImplSpecs() const override {
        // return Registrar<LSQImpl_cuda>::getKeys(); // Note: cannot return set due to python binding 
        std::set<ImplSpec> implSpecsSet = Registrar<LSQImpl_cuda>::getKeys();
        return std::vector<ImplSpec>(implSpecsSet.begin(), implSpecsSet.end());
    }

    void forward() override;
    void backward() override;
    ~LSQImpl_cuda();

private:
    size_t mWorkspaceSize = 0;
    void* mWorkspace = nullptr;

};

// Implementation entry point registration to Operator
REGISTRAR(LSQ_Op, "cuda", Aidge::LSQImpl_cuda::create);

}  // namespace Aidge

#endif /* AIDGE_CUDA_OPERATOR_LSQIMPL_H_ */

