/********************************************************************************
 * Copyright (c) 2024 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CUDA_OPERATOR_LSQIMPL_KERNELS_H_
#define AIDGE_CUDA_OPERATOR_LSQIMPL_KERNELS_H_

#include "aidge/data/Data.hpp"

#include "aidge/backend/cuda/utils/CudaUtils.hpp" 
#include "aidge/backend/cuda/operator/LSQImpl.hpp"

namespace Aidge {
template <class I, class O>
void LSQImpl_cuda_forward_kernel(std::size_t inputLength,
                                const std::pair<int, int>& range,
                                const void* input_,
                                const void* stepSize_,
                                void* output_);

template <class I, class GI, class GO>
void LSQImpl_cuda_backward_kernel(const std::size_t inputLength,
                                 const std::pair<int, int>& range,
                                 const void* input_,
                                 const void* stepSize_,
                                 const void* grad_output_,
                                 void* grad_input_,
                                 void* grad_stepSize_,
                                 void* grad_workspace_);


// Kernels registration to implementation entry point

//REGISTRAR(LSQImpl_cuda,
//    {{DataType::Float16, DataFormat::NCHW}, {DataType::Float16, DataFormat::NCHW}},
//    {ProdConso::inPlaceModel, Aidge::LSQImpl_cuda_forward_kernel<half_float::half, half_float::half>, Aidge::LSQImpl_cuda_backward_kernel<half_float::half, half_float::half, half_float::half>});

REGISTRAR(LSQImpl_cuda,
    {{DataType::Float32, DataFormat::NCHW}, {DataType::Float32, DataFormat::NCHW}},
    {ProdConso::inPlaceModel, Aidge::LSQImpl_cuda_forward_kernel<float, float>, Aidge::LSQImpl_cuda_backward_kernel<float, float, float>});

REGISTRAR(LSQImpl_cuda,
    {{DataType::Float64, DataFormat::NCHW}, {DataType::Float64, DataFormat::NCHW}},
    {ProdConso::inPlaceModel, Aidge::LSQImpl_cuda_forward_kernel<double, double>, Aidge::LSQImpl_cuda_backward_kernel<double, double, double>});

}  // namespace Aidge

#endif /* AIDGE_CUDA_OPERATOR_LSQIMPL_KERNELS_H_ */
