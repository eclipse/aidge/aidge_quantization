// /********************************************************************************
//  * Copyright (c) 2023 CEA-List
//  *
//  * This program and the accompanying materials are made available under the
//  * terms of the Eclipse Public License 2.0 which is available at
//  * http://www.eclipse.org/legal/epl-2.0.
//  *
//  * SPDX-License-Identifier: EPL-2.0
//  *
//  ********************************************************************************/
#ifndef AIDGE_QUANTIZATION_CPU_IMPORTS_H_
#define AIDGE_QUANTIZATION_CPU_IMPORTS_H_

#include "aidge/backend/cpu/operator/FixedQImpl.hpp"
#include "aidge/backend/cpu/operator/LSQImpl.hpp"

#include "aidge/backend/cpu/operator/SAT/TanhClampImpl.hpp"
#include "aidge/backend/cpu/operator/SAT/DoReFaImpl.hpp"

// ...

#endif /* AIDGE_QUANTIZATION_CPU_IMPORTS_H_ */