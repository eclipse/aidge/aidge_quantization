/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/
#ifndef AIDGE_QUANTIZATION_QUANTIZATION_PTQ_PTQMETAOPS_H_
#define AIDGE_QUANTIZATION_QUANTIZATION_PTQ_PTQMETAOPS_H_

#include <memory>
#include <string>

#include "aidge/graph/GraphView.hpp"
#include "aidge/graph/Node.hpp"

namespace Aidge {

/// @brief Quantizer acts as a meta-operator to handle scaling operations in the PTQ, replacing the Scaling Operator.
/// This operator is composed of a sequence of [Mul] -> [Clip] -> [Round] operations.
///
/// @param scalingFactor The scaling factor to apply to the input (essentially a scalar to multiply the input with).
/// @param clip_min The minimum value for the clip operation.
/// @param clip_max The maximum value for the clip operation.
/// @param name The name of the meta-operator node created.
/// @return A shared pointer to an instance of the meta-operator node.
std::shared_ptr<Aidge::Node> Quantizer(double scalingFactor, double clipMin, double clipMax, const std::string& name);

/// @brief The purpose of Scaling is to encapsulate the Mul operator and tag it as a PTQ node rather than a regular Mul operator.
/// Therefore, this meta-operator consists solely of a [Mul] operation.
///
/// @param scalingFactor The scaling factor to apply to the input (a scalar to multiply the input with).
/// @param name The name of the meta-operator node created.
/// @return A shared pointer to an instance of the scaling node.
std::shared_ptr<Aidge::Node> Scaling(double scalingFactor, const std::string& name = "");

/// @brief Updates the scaling factor of a PTQ meta-operator node, allowing for dynamic adjustment of the scaling parameter.
/// This function sets a new scaling factor for a specified meta-operator node, modifying the scalar applied in the [Mul] operation.
/// The meta-operator node must be a PTQ-specific operator, such as a Quantizer or Scaling node.
///
/// @param MetaOpNode A shared pointer to the PTQ meta-operator node whose scaling factor will be updated.
/// @param newScalingFactor The new scaling factor to apply to the meta-operator node.
/// @return True if the scaling factor was successfully updated, false if the operation failed (e.g., if MetaOpNode is null or incompatible).
void updateScalingFactor(std::shared_ptr<Aidge::Node> MetaOpNode, double newScalingFactor);

/// @brief Retrieves the current scaling factor of a PTQ meta-operator node.
/// This function returns the scaling factor associated with the specified PTQ meta-operator node,
/// allowing inspection of the current scalar applied in the [Mul] operation.
///
/// @param MetaOpNode A shared pointer to the PTQ meta-operator node whose scaling factor is being queried.
/// @return The scaling factor currently applied to the meta-operator node, or -1 if the operation fails (e.g., if MetaOpNode is null or incompatible).
double getScalingFactor(std::shared_ptr<Aidge::Node> MetaOpNode);

/// @brief Sets the clip range for an existing Quantizer node by specifying minimum and maximum clipping values.
/// This function modifies the clip range of a Quantizer node, allowing adjustment of the range within which values are clipped
/// in the [Clip] operation of the Quantizer sequence.
///
/// @param QuantizerNode A shared pointer to the Quantizer node whose clip range is being set.
/// This node should have been created using the Quantizer function.
/// @param min The minimum value for the clip range. Values below this will be clipped to this minimum.
/// @param max The maximum value for the clip range. Values above this will be clipped to this maximum.
/// @return True if the clip range was successfully set, false if the operation failed (e.g., if QuantizerNode is null).
void setClipRange(std::shared_ptr<Aidge::Node> QuantizerNode, double min, double max);

}

#endif /* AIDGE_QUANTIZATION_QUANTIZATION_PTQ_PTQMETAOPS_H_ */
