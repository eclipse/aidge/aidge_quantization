/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_QUANTIZATION_QUANTIZATION_PTQ_CLE_H_
#define AIDGE_QUANTIZATION_QUANTIZATION_PTQ_CLE_H_

#include <memory>

#include "aidge/graph/GraphView.hpp"

namespace Aidge
{

    /**
     * @brief Equalize the ranges of the nodes parameters by proceding iteratively.
     * Can only be applied to single branch networks (otherwise does not edit the GraphView).
     *
     * Cross Layer Equalization (CLE) is used to balance the weights between consecutive
     * layers to improve quantization performance. It works by iteratively scaling weights
     * and biases of adjacent layers while preserving the overall function of the network.
     *
     * @note The operation modifies weights and biases in-place but preserves the mathematical
     * function computed by the network.
     *
     * @param graphView The GraphView to process.
     * @param targetDelta the stopping criterion (typical value : 0.01). Smaller values lead
     *                    to more precise equalization but may require more iterations.
     */
    void crossLayerEqualization(std::shared_ptr<GraphView> graphView, double targetDelta = 0.01);

}  // namespace Aidge

#endif /* AIDGE_QUANTIZATION_QUANTIZATION_PTQ_CLE_H_ */
