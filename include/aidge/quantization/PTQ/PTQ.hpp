/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_QUANTIZATION_QUANTIZATION_PTQ_PTQ_H_
#define AIDGE_QUANTIZATION_QUANTIZATION_PTQ_PTQ_H_

#include <cstdint>  // std::uint8_t
#include <map>
#include <memory>
#include <set>
#include <string>
#include <utility>  // std::pair
#include <vector>

#include "aidge/data/Tensor.hpp"
#include "aidge/quantization/PTQ/Clipping.hpp"
#include "aidge/graph/GraphView.hpp"

namespace Aidge {

    /**
     * @brief Set of the types of the nodes which contain affine transforms (that is Y = A.X + B)
     */
    static const std::set<std::string> affineNodeTypes({"FC", "Conv2D", "ConvDepthWise2D", "PaddedConv2D", "PaddedConvDepthWise2D"});

    /**
     * @brief Set of the types of the nodes which does not affect the PTQ process
     */
    static const std::set<std::string> seamlessNodeTypes({"LeakyReLU", "Pad2D", "MaxPooling2D", "AvgPooling2D", "PaddedMaxPooling2D", "PaddedAvgPooling2D", "GlobalAveragePooling", "Reshape", "Transpose", "Gather", "Resize"});

    /**
     * @brief Set of the types of the nodes that merge multiple branches into one
     */
    static const std::set<std::string> mergingNodeTypes({"Add", "Concat", "Sub"});

    /**
     * @brief Determine if a node contains an affine transform (that is Y = A.X + B)
     * @param node The node to be checked
     * @return True if the node is affine, else false.
     */
    bool isAffine(std::shared_ptr<Node> node);

    /**
     * @brief Determine if a node contains an operator that does not affect the PTQ process
     * @param node The node to be checked
     * @return True if the node is seamless, else false.
     */
    bool isSeamless(std::shared_ptr<Node> node);

    /**
     * @brief Determine if a node contains an operator that merges multiple branches into one
     * @param node The node to be checked
     * @return True if the node is merging, else false.
     */
    bool isMerging(std::shared_ptr<Node> node);

    /**
     * @brief Retrieve the scheduled vector of node of a graphView, without the Producer nodes.
     * @param graphView The graphView containing the nodes
     * @param verbose Whether to print the node vector or not
     * @return The scheduled vector of nodes
     */
    std::vector<std::shared_ptr<Node>> retrieveNodeVector(std::shared_ptr<GraphView> graphView, bool newSchedule = true, bool verbose = false);

    /**
     * @brief Determine whether an input GraphView can be quantized or not.
     * @param graphView The GraphView to be checked.
     * @return True if the GraphView can be quantized, else false.
     */
    bool checkArchitecture(std::shared_ptr<GraphView> graphView);


    void prepareNetwork(std::shared_ptr<GraphView> graphView);


    /**
     * @brief Insert a scaling node after each affine node of the GraphView.
     * Also insert a scaling node in every purely residual branches.
     * @param graphView The GraphView containing the affine nodes.
     */
    void insertScalingNodes(std::shared_ptr<GraphView> graphView);

    /**
     * @brief Normalize the parameters of each parametrized node, so that they fit in the [-1:1] range.
     * @param graphView The GraphView containing the parametrized nodes.
     */
    void normalizeParameters(std::shared_ptr<GraphView> graphView);

    /**
     * @brief Compute the activation ranges of every affine node, given an input dataset.
     * @param graphView The GraphView containing the affine nodes, on which the inferences are performed.
     * @param inputDataSet The input dataset, consisting of a vector of input samples.
     * @param scalingNodesOnly Whether to restrain the retreival of the ranges to scaling nodes only or not.
     * @return A map associating each affine node name to it's corresponding output range.
     */
    std::map<std::string, double> computeRanges(std::shared_ptr<GraphView> graphView, std::vector<std::shared_ptr<Tensor>> inputDataSet, bool scalingNodesOnly, bool useCuda);

    /**
     * @brief Normalize the activations of each affine node so that they fit in the [-1:1] range.
     * This is done by reconfiguring the scaling nodes, as well as rescaling the weights and biases tensors.
     * @param graphView The GraphView containing the affine nodes.
     * @param valueRanges The node output value ranges computed over the calibration dataset.
     */
    void normalizeActivations(std::shared_ptr<GraphView> graphView, std::map<std::string, double> valueRanges);

    /**
     * @brief For each node, compute the sign of its input and output values.
     * The goal of the routine is to maximize the number of unsigned IOs in order to double the value resolution when possible.
     * @param graphView The GraphView to analyze.
     * @param verbose Whether to print the sign map or not.
     * @return A map associating a pair of sign to each node of the GraphView (a sign for the input and one for the output).
     */
    std::map<std::string, std::pair<bool, bool>> computeSignMap(std::shared_ptr<GraphView> graphView, bool verbose);

    /**
     * @brief Quantize an already normalized (in term of parameters and activations) network.
     * @param graphView The GraphView to be quantized.
     * @param nbBits The desired number of bits of the quantization.
     * @param applyRounding Whether to apply the rounding operations or not.
     * @param optimizeSigns Whether to take account of the IO signs of the operators or not.
     * @param verbose Whether to print the sign map or not.
     */
    void quantizeNormalizedNetwork(std::shared_ptr<GraphView> graphView, std::uint8_t nbBits, bool applyRounding, bool optimizeSigns, bool verbose);

    /**
     * @brief Main quantization routine. Performs every step of the quantization pipeline.
     * @param graphView The GraphView to be quantized.
     * @param nbBits The desired number of bits of the quantization.
     * @param inputDataSet The input dataset on which the value ranges are computed.
     * @param clippingMode: Type of the clipping optimization. Can be either 'MAX', 'MSE', 'AA' or 'KL'.
     * @param applyRounding Whether to apply the rounding operations or not.
     * @param optimizeSigns Whether to take account of the IO signs of the operators or not.
     * @param singleShift Whether to convert the scaling factors into powers of two. If true the approximations are compensated using the previous nodes weights.
     * @param verbose Whether to print internal informations about the quantization process.
     */
    void quantizeNetwork(std::shared_ptr<GraphView> graphView, std::uint8_t nbBits, std::vector<std::shared_ptr<Tensor>> inputDataSet, Clipping clippingMode, bool applyRounding, bool optimizeSigns, bool singleShift, bool useCuda, bool verbose);

    /**
     * @brief Compute the weight ranges of every affine node. Provided for debugging purposes.
     * @param graphView The GraphView containing the affine nodes.
     * @return A map associating each affine node name to it's corresponding weight range.
     */
    std::map<std::string, double> getWeightRanges(std::shared_ptr<GraphView> graphView);

    /**
     * @brief Clear the affine nodes biases. Provided form debugging purposes.
     * @param graphView The GraphView containing the affine nodes.
     */
    void clearBiases(std::shared_ptr<GraphView> graphView);

    /**
     * @brief Developement and test routine.
     * @param graphView The GraphView under test.
     */
    void devPTQ(std::shared_ptr<GraphView> graphView);
}

#endif /* AIDGE_QUANTIZATION_QUANTIZATION_PTQ_PTQ_H_ */

