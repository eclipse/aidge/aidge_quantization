/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_QUANTIZATION_QUANTIZATION_PTQ_CLIP_H_
#define AIDGE_QUANTIZATION_QUANTIZATION_PTQ_CLIP_H_

#include <cstdint>  // std::uint8_t
#include <map>
#include <memory>
#include <string>
#include <vector>

#include "aidge/data/Tensor.hpp"
#include "aidge/graph/GraphView.hpp"

namespace Aidge
{
    /**
     * @brief Kind of clipping policy to apply during the activation quantization
     */
    enum class Clipping {MAX = 1, MSE, AA, KL};

    /**
     * @brief Compute the histograms of the activations of each node contained in the map of the ranges (passed as argument).
     * @param valueRanges A map associating each considered node name to its corresponding output range.
     * @param nbBins Desired number of bins of the returned histograms.
     * @param graphView The GraphView containing the considered nodes.
     * @param inputDataSet The input dataset, consisting of a vector of input samples.
     * @return A map associating each node name to it's corresponding activation histogram.
     */
    std::map<std::string, std::vector<int>> computeHistograms(std::map<std::string, double> valueRanges, int nbBins, std::shared_ptr<GraphView> graphView, std::vector<std::shared_ptr<Tensor>> inputDataSet, bool useCuda);

    /**
     * @brief Given an input activation histogram, compute the optimal clipping value in the sense of the Lp norm.
     * @param histogram: The provided activation histogram.
     * @param nbBits: The quantization number of bits.
     * @param exponent: The exponent of the Lp norm (e.g. 2 for the MSE).
     * @return The optimal clipping value.
     */
    double computeMEClipping(std::vector<int> histogram, std::uint8_t nbBits, double exponent);

    /**
     * @brief Given an input activation histogram, compute the optimal clipping value in the sense of the KL divergence.
     * @param histogram: The provided activation histogram.
     * @param nbBits: The quantization number of bits.
     * @return The optimal clipping value.
     */
    double computeKLClipping(std::vector<int> histogram, std::uint8_t nbBits);

    /**
     * @brief Return a corrected map of the provided activation ranges.
     * To do so compute the optimal clipping values for every node and multiply the input ranges by those values.
     * The method used to compute the clippings can be eihter 'MSE', 'AA', 'KL' or 'MAX'.
     * @param clippingMode The method used to compute the optimal clippings.
     * @param valueRanges The map associating each affine node to its output range.
     * @param nbBits The quantization number of bits.
     * @param graphView The GraphView containing the considered nodes.
     * @param inputDataSet The input dataset, consisting of a vector of input samples.
     * @param verbose Whether to print the clipping values or not.
     * @return The corrected map associating each provided node to its clipped range.
     */
    std::map<std::string, double> adjustRanges(Clipping clippingMode, std::map<std::string, double> valueRanges, std::uint8_t nbBits, std::shared_ptr<GraphView> graphView, std::vector<std::shared_ptr<Tensor>> inputDataSet, bool useCuda, bool verbose);

}

#endif /* AIDGE_QUANTIZATION_QUANTIZATION_PTQ_CLIP_H_ */

