/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_QUANTIZATION_QUANTIZATION_QAT_LSQ_H_
#define AIDGE_QUANTIZATION_QUANTIZATION_QAT_LSQ_H_

#include <cstddef>  // std::size_t
#include <memory>

#include "aidge/data/Tensor.hpp"
#include "aidge/graph/GraphView.hpp"

namespace Aidge {
namespace QuantLSQ {

/**
 * @brief Insert the LSQ quantizer nodes in a given GraphView
 * @param graphView The GraphView containing the graph to quantize.
 * @param nbBits Number of quantization bits.
 * @param span Fixed output span of the quantizers.
 */
void insertQuantizers(std::shared_ptr<GraphView> graphView, std::size_t nbBits, float step_size);

/**
 * @brief Given a GraphView with parameters properly initialized and some calibration data,
 * insert the LSQ quantizer nodes, and adjust their step-sizes.
 * @param graphView The GraphView containing the graph to quantize.
 * @param nbBits Number of quantization bits.
 * @param calibrationData Calibration data used to adjust the spans.
 * @param scale Multiplicative constant applied to the spans.
 */
void insertAndInitQuantizers(std::shared_ptr<GraphView> graphView, std::size_t nbBits, std::shared_ptr<Tensor> calibrationData);

}  // namespace QuantLSQ
}  // namespace Aidge

#endif /* AIDGE_QUANTIZATION_QUANTIZATION_QAT_LSQ_H_ */

