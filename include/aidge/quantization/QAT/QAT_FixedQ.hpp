/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_QUANTIZATION_QUANTIZATION_QAT_FIXEDQ_H_
#define AIDGE_QUANTIZATION_QUANTIZATION_QAT_FIXEDQ_H_

#include <memory>

#include "aidge/graph/Node.hpp"
#include "aidge/graph/GraphView.hpp"
#include "aidge/data/Tensor.hpp"

namespace Aidge {
namespace QuantFixedQ {

/**
 * @brief Insert the FixedQ quantizer nodes in a given GraphView
 * @param graphView The GraphView containing the graph to quantize.
 * @param nbBits Number of quantization bits.
 * @param span Fixed output span of the quantizers.
 */
void insertQuantizers(std::shared_ptr<GraphView> graphView, size_t nbBits, float span);

/**
 * @brief Given a GraphView with parameters properly initialized and some calibration data,
 * insert the FixedQ quantizer nodes, and adjust their output spans.
 * @param graphView The GraphView containing the graph to quantize.
 * @param nbBits Number of quantization bits.
 * @param calibrationData Calibration data used to adjust the spans.
 * @param scale Multiplicative constant applied to the spans.
 */
void insertAndInitQuantizers(std::shared_ptr<GraphView> graphView, size_t nbBits, std::shared_ptr<Tensor> calibrationData, float scale);

/**
 * @brief Developement and test routine.
 * @param graphView The GraphView under test.
 */
void devQAT(std::shared_ptr<GraphView> graphView);

}
}

#endif /* AIDGE_QUANTIZATION_QUANTIZATION_QAT_FIXEDQ_H_ */

