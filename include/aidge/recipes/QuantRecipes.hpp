/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_QUANTIZATION_QUANTRECIPES_H_
#define AIDGE_QUANTIZATION_QUANTRECIPES_H_

#include "aidge/data/Tensor.hpp"
#include "aidge/graph/GraphView.hpp"

namespace Aidge 
{
    /**
     * @brief Remove all the SoftMax leaf nodes of a GraphView.
     * @param graphView The GraphView to process.
     */
    void popSoftMax(std::shared_ptr<GraphView> graphView);

    /**
     * @brief Insert a BatchNorm node after every Convolutional node of a GraphView
     * @param graphView The GraphView to process.
     */
    void insertBatchNormNodes(std::shared_ptr<GraphView> graphView);

    /**
     * @brief Given a base name, create a node name that is unique in a considered GraphView
     * @param graphView The GraphView to process.
     */
    std::string makeUniqueName(std::string baseName, std::shared_ptr<GraphView> graphView);

    /**
     * @brief Given a GraphView, ensure that each node name is unique
     * @param graphView The GraphView to process.
     */
    void sanitizeNodeNames(std::shared_ptr<GraphView> graphView);
}

#endif /* AIDGE_QUANTIZATION_QUANTRECIPES_H_ */
