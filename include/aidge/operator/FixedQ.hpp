/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_QUANTIZATION_OPERATOR_FIXEDQ_H_
#define AIDGE_QUANTIZATION_OPERATOR_FIXEDQ_H_

#include <cstddef>  // std::size_t
#include <memory>
#include <string>
#include <vector>

#include "aidge/backend/OperatorImpl.hpp"
#include "aidge/graph/Node.hpp"
#include "aidge/operator/OperatorTensor.hpp"
#include "aidge/utils/ErrorHandling.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/StaticAttributes.hpp"
#include "aidge/utils/Types.h"


namespace Aidge {

enum class FixedQAttr { NbBits, Span, IsOutputUnsigned };

class FixedQ_Op : public OperatorTensor,
    public Registrable<FixedQ_Op, std::string,
    std::function<std::shared_ptr<OperatorImpl>(const FixedQ_Op&)>> {

public:
    static const std::string Type;

private:
    using Attributes_ = StaticAttributes<FixedQAttr, std::size_t, float, bool>;
    template <FixedQAttr e> using attr = typename Attributes_::template attr<e>;
    const std::shared_ptr<Attributes_> mAttributes;

public:

    FixedQ_Op(std::size_t nbBits = 8, float span = 4.0f, bool isOutputUnsigned = false) :
      OperatorTensor(Type, {InputCategory::Data}, 1),
      mAttributes(std::make_shared<Attributes_>(
        attr<FixedQAttr::NbBits>(nbBits),
        attr<FixedQAttr::Span>(span),
        attr<FixedQAttr::IsOutputUnsigned>(isOutputUnsigned)))
    {}

    /**
     * @brief Copy-constructor. Copy the operator attributes and its output
     * tensor(s), but not its input tensors (the new operator has no input associated).
     * @param op Operator to copy.
     */
    FixedQ_Op(const FixedQ_Op& op);

    /**
     * @brief Clone the operator using its copy-constructor.
     * @see Operator::FixedQ_Op
     */
    std::shared_ptr<Operator> clone() const override {
        return std::make_shared<FixedQ_Op>(*this);
    }
    std::set<std::string> getAvailableBackends() const override final;
    void setBackend(const std::string& name, DeviceIdx_t device = 0) override final;

    inline std::shared_ptr<Attributes> attributes() const override { return mAttributes; }
    inline std::size_t& nbBits() const noexcept { return mAttributes->getAttr<FixedQAttr::NbBits>(); }
    inline float& span() const noexcept { return mAttributes->getAttr<FixedQAttr::Span>(); }
    inline bool& isOutputUnsigned() const noexcept { return mAttributes->getAttr<FixedQAttr::IsOutputUnsigned>(); }


    static const std::vector<std::string> getInputsName(){
        return {"data_input"};
    }
    static const std::vector<std::string> getOutputsName(){
        return {"data_output"};
    }


};

std::shared_ptr<Node> FixedQ(std::size_t nbBits = 8,
                            float span = 4.0f,
                            bool isOutputUnsigned = false,
                            const std::string& name = "");

}  // namespace Aidge

namespace {
template <>
const char* const EnumStrings<Aidge::FixedQAttr>::data[] = {"nb_bits", "span", "is_output_unsigned"};
}

#endif /* AIDGE_QUANTIZATION_OPERATOR_FIXEDQ_H_ */
