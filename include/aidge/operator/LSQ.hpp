/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_QUANTIZATION_OPERATOR_LSQ_H_
#define AIDGE_QUANTIZATION_OPERATOR_LSQ_H_

#include <cassert>
#include <memory>
#include <vector>

#include "aidge/backend/OperatorImpl.hpp"
#include "aidge/graph/Node.hpp"
#include "aidge/operator/OperatorTensor.hpp"
#include "aidge/operator/Producer.hpp"
#include "aidge/utils/ErrorHandling.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/StaticAttributes.hpp"
#include "aidge/utils/Types.h"

namespace Aidge {

enum class LSQAttr { Range };

/**
 * LSQ is the weights AND activations quantizer for the LSQ method.
 */
class LSQ_Op : public OperatorTensor,
    public Registrable<LSQ_Op, std::string, std::function<std::shared_ptr<OperatorImpl>(const LSQ_Op &)>> {
public:
    static const std::string Type;

private:
    using Attributes_ = StaticAttributes<LSQAttr, std::pair<int, int>>;
    template <LSQAttr e> using attr = typename Attributes_::template attr<e>;
    const std::shared_ptr<Attributes_> mAttributes;

public:
    LSQ_Op(const std::pair<int, int>& range = {0, 255})
      : OperatorTensor(Type, {InputCategory::Data, InputCategory::Param}, 1),
        mAttributes(std::make_shared<Attributes_>(
            attr<LSQAttr::Range>(range)))
    {}

    /**
     * @brief Copy-constructor. Copy the operator attributes and its output tensor(s), but not its input tensors (the new operator has no input associated).
     * @param op Operator to copy.
     */
    LSQ_Op(const LSQ_Op& op)
        : OperatorTensor(op),
          mAttributes(op.mAttributes)
    {
        if (op.mImpl){
            SET_IMPL_MACRO(LSQ_Op, *this, op.backend());
        }else{
            mImpl = nullptr;
        }
    }

    /**
     * @brief Clone the operator using its copy-constructor.
     * @see Operator::LSQ_Op
     */
    std::shared_ptr<Operator> clone() const override {
        return std::make_shared<LSQ_Op>(*this);
    }

    bool forwardDims(bool allowDataDependency = false) override final;
    std::set<std::string> getAvailableBackends() const override final;
    void setBackend(const std::string& name, DeviceIdx_t device = 0) override final;

    inline std::shared_ptr<Attributes> attributes() const override { return mAttributes; }
    inline std::pair<int, int>& range() const noexcept { return mAttributes->getAttr<LSQAttr::Range>(); }

    static const std::vector<std::string> getInputsName(){
        return {"data_input", "step_size"};
    }
    static const std::vector<std::string> getOutputsName(){
        return {"data_output"};
    }

};

/**
 * Range should be (with N the number of bits):
 * - {0, 2^N - 1} in place of ReLU activations
 * - {-2^(N-1), 2^(N-1) - 1} in for weights quantization
 */
inline std::shared_ptr<Node> LSQ(const std::pair<int, int>& range = {0, 255}, const std::string& name = "") {
    auto lsq = std::make_shared<Node>(std::make_shared<LSQ_Op>(range), name);
    addProducer<1>(lsq, 1, {1}, "ss");
    return lsq;
}
}

namespace {
template <>
const char *const EnumStrings<Aidge::LSQAttr>::data[] = {"range"};
}

#endif /* AIDGE_QUANTIZATION_OPERATOR_LSQ_H_ */
