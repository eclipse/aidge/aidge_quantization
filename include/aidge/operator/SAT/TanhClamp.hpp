/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_QUANTIZATION_OPERATOR_SAT_TANHCLAMP_H_
#define AIDGE_QUANTIZATION_OPERATOR_SAT_TANHCLAMP_H_

#include <memory>
#include <set>
#include <string>
#include <vector>

#include "aidge/backend/OperatorImpl.hpp"
#include "aidge/graph/Node.hpp"
#include "aidge/operator/OperatorTensor.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"

namespace Aidge {

/**
 * TanhClamp is the weights clamping for the 1st training phase (clamping) of the SAT method.
 */
class TanhClamp_Op : public OperatorTensor,
    public Registrable<TanhClamp_Op, std::string, std::function<std::shared_ptr<OperatorImpl>(const TanhClamp_Op&)>> {

public:
    static const std::string Type;

    TanhClamp_Op()
      : OperatorTensor(Type, {InputCategory::Data}, 2)
    {}

    /**
     * @brief Copy-constructor. Copy the operator attributes and its output tensor(s), but not its input tensors (the new operator has no input associated).
     * @param op Operator to copy.
     */
    TanhClamp_Op(const TanhClamp_Op& op);

    /**
     * @brief Clone the operator using its copy-constructor.
     * @see Operator::TanhClamp_Op
     */
    std::shared_ptr<Operator> clone() const override;

    bool forwardDims(bool allowDataDependency = false) override final;
    std::set<std::string> getAvailableBackends() const override final;

    void setBackend(const std::string& name, DeviceIdx_t device = 0) override final;

    static const std::vector<std::string> getInputsName(){
        return {"data_input"};
    }
    static const std::vector<std::string> getOutputsName(){
        return {"data_output", "scaling"};
    }
};

std::shared_ptr<Node> TanhClamp(const std::string& name = "");

}  // namespace Aidge

#endif /* AIDGE_QUANTIZATION_OPERATOR_SAT_TANHCLAMP_H_ */
