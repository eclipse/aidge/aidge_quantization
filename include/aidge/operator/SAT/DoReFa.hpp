/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_QUANTIZATION_OPERATOR_SAT_DOREFA_H_
#define AIDGE_QUANTIZATION_OPERATOR_SAT_DOREFA_H_

#include <memory>
#include <vector>

#include "aidge/backend/OperatorImpl.hpp"
#include "aidge/graph/Node.hpp"
#include "aidge/operator/OperatorTensor.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/StaticAttributes.hpp"
#include "aidge/utils/Types.h"

namespace Aidge {

enum class DoReFaAttr { Range, Mode };
enum class DoReFaMode {
    Default,        // Original SAT paper (not including 0)
    Symmetric,      // Symmetric range including 0
    //Asymmetric,
    //FullRange
};

/**
 * DoReFa is the weights quantizer for the 2nd training phase (quantization) of the SAT method.
 */
class DoReFa_Op : public OperatorTensor,
    public Registrable<DoReFa_Op, std::string, std::function<std::shared_ptr<OperatorImpl>(const DoReFa_Op&)>> {
public:
    static const std::string Type;

private:
    using Attributes_ = StaticAttributes<DoReFaAttr, std::size_t, DoReFaMode>;
    template <DoReFaAttr e> using attr = typename Attributes_::template attr<e>;
    const std::shared_ptr<Attributes_> mAttributes;

public:
    /**
     * @brief Constructor for DoReFa_Op
     * @param range The quantization range (default: 255)
     * @param mode The quantization mode (default: Default)
     */
    DoReFa_Op(std::size_t range = 255, DoReFaMode mode = DoReFaMode::Default)
      : OperatorTensor(Type, {InputCategory::Param}, 1),
        mAttributes(std::make_shared<Attributes_>(
            attr<DoReFaAttr::Range>(range),
            attr<DoReFaAttr::Mode>(mode)))
    {}

    /**
     * @brief Copy-constructor. Copy the operator attributes and its output tensor(s), but not its input tensors (the new operator has no input associated).
     * @param op Operator to copy.
     */
    DoReFa_Op(const DoReFa_Op& op);

    /**
     * @brief Clone the operator using its copy-constructor.
     * @see Operator::DoReFa_Op
     * @return std::shared_ptr<Operator> A deep copy of the operator
     */
    std::shared_ptr<Operator> clone() const override;

    /**
     * @brief Get available backends for this operator
     * @return std::set<std::string> Set of supported backend names
     */
    std::set<std::string> getAvailableBackends() const override final;

    /**
     * @brief Set the backend for this operator
     * @param name Backend name
     * @param device Device index (default: 0)
     */
    void setBackend(const std::string& name, DeviceIdx_t device = 0) override final;

    /**
     * @brief Get operator attributes
     * @return std::shared_ptr<Attributes> Shared pointer to operator attributes
     */
    inline std::shared_ptr<Attributes> attributes() const override { return mAttributes; }
    inline std::size_t& range() const noexcept { return mAttributes->getAttr<DoReFaAttr::Range>(); }
    inline DoReFaMode& mode() const noexcept { return mAttributes->getAttr<DoReFaAttr::Mode>(); }

    static const std::vector<std::string> getInputsName(){
        return {"data_input"};
    }
    static const std::vector<std::string> getOutputsName(){
        return {"data_output"};
    }
};

/**
 * @brief Factory function to create a DoReFa operator node
 *
 * @param range Quantization range (default: 255)
 * @param mode Quantization mode (default: Default)
 * @param name Node name (default: empty)
 *
 * @return std::shared_ptr<Node> Shared pointer to the created node
 */
std::shared_ptr<Node> DoReFa(std::size_t range = 255,
                             DoReFaMode mode = DoReFaMode::Default,
                             const std::string& name = "");

}  // namespace Aidge

namespace {
template <>
const char *const EnumStrings<Aidge::DoReFaAttr>::data[] = {"range", "mode"};

template <>
const char *const EnumStrings<Aidge::DoReFaMode>::data[] = {"default", "symmetric", "asymmetric", "full_range"};
}

#endif /* AIDGE_QUANTIZATION_OPERATOR_SAT_DOREFA_H_ */
