#ifndef VERSION_H
#define VERSION_H

namespace Aidge {
static constexpr const int PROJECT_VERSION_MAJOR = 0;
static constexpr const int PROJECT_VERSION_MINOR = 2;
static constexpr const int PROJECT_VERSION_PATCH = 0;
static constexpr const char * PROJECT_VERSION = "0.2.0";
static constexpr const char * PROJECT_GIT_HASH = "f50c860";
}
#endif // VERSION_H
