import unittest
import gzip
import numpy as np
from pathlib import Path

import aidge_core
import aidge_backend_cpu
import aidge_onnx
import aidge_quantization

from aidge_core import Log, Level

# --------------------------------------------------------------
# CONFIGS
# --------------------------------------------------------------

NB_SAMPLES      = 1000               # max : 1000
SAMPLE_SHAPE    = (1, 1, 28, 28)
MODEL_NAME      = 'MiniResNet.onnx'  # 'ConvNet.onnx'
ACCURACIES      = (95.4, 94.4)       # (97.9, 97.7)
NB_BITS         = 4

# --------------------------------------------------------------
# UTILS
# --------------------------------------------------------------

def propagate(model, scheduler, sample):
    input_tensor = aidge_core.Tensor(sample)
    scheduler.forward(True, [input_tensor])
    output_node = model.get_output_nodes().pop()
    output_tensor = output_node.get_operator().get_output(0)
    return np.array(output_tensor)

def prepare_sample(sample):
    sample = np.reshape(sample, SAMPLE_SHAPE)
    return sample.astype('float32')

def compute_accuracy(model, samples, labels):
    acc = 0
    scheduler = aidge_core.SequentialScheduler(model)
    for i, sample in enumerate(samples):
        x = prepare_sample(sample)
        y = propagate(model, scheduler, x)
        if labels[i] == np.argmax(y):
            acc += 1
    return acc / len(samples)

# --------------------------------------------------------------
# TEST CLASS
# --------------------------------------------------------------

class test_ptq(unittest.TestCase):

    def setUp(self):

        # load the samples / labels (numpy)

        curr_file_dir = Path(__file__).parent.resolve()
        self.samples = np.load(gzip.GzipFile(curr_file_dir / 'assets/mnist_samples.npy.gz', "r"))
        self.labels  = np.load(gzip.GzipFile(curr_file_dir / 'assets/mnist_labels.npy.gz',  "r"))

        # load the model in AIDGE

        self.model = aidge_onnx.load_onnx(curr_file_dir / "assets/" / MODEL_NAME, verbose=False)
        aidge_core.remove_flatten(self.model)

        self.model.set_datatype(aidge_core.dtype.float32)
        self.model.set_backend("cpu")

    def tearDown(self):
        pass


    def test_model(self):

        Log.set_console_level(Level.Info)
        # compute the base accuracy
        accuracy = compute_accuracy(self.model, self.samples[0:NB_SAMPLES], self.labels)
        self.assertAlmostEqual(accuracy * 100, ACCURACIES[0], msg='base accuracy does not meet the baseline !', delta=0.1)

    def test_quant_model(self):

        Log.set_console_level(Level.Debug)

        # create the calibration dataset

        tensors = []
        for sample in self.samples[0:NB_SAMPLES]:
            sample = prepare_sample(sample)
            tensor = aidge_core.Tensor(sample)
            tensors.append(tensor)

        # quantize the model

        aidge_quantization.quantize_network(
            self.model,
            NB_BITS,
            tensors,
            clipping_mode=aidge_quantization.Clipping.MSE,
            no_quantization=False,
            optimize_signs=True,
            single_shift=False
        )

        # rescale the inputs

        scaling = 2**(NB_BITS-1)-1
        for i in range(NB_SAMPLES):
            self.samples[i] = self.samples[i]*scaling # XXX np.round ???

        # compute the quantized accuracy

        accuracy = compute_accuracy(self.model, self.samples, self.labels)
        self.assertAlmostEqual(accuracy * 100, ACCURACIES[1], msg='quantized accuracy does not meet the baseline !', delta=0.1)


if __name__ == '__main__':
    unittest.main()
